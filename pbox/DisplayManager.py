# coding:utf-8
import time
import os
import cv2
from multiprocessing import Process
import numpy as np
import threading
import subprocess
from PIL import Image, ImageDraw, ImageFont
from imutils.video import WebcamVideoStream
from imutils.video import FPS
#=============Global Variable Start=======================
CHINESE_FONT = './font/msjhbd.ttf'
ERROR_MODE="./img/try_cross.png"
SUCCESS_SMALL_MODE="./img/try_tick_one.png"
START_VI = 'Version 1.0.9.0'
START_PGN='./img/start.png'
SERIAL_NUMBER = '001AB910006'
DOWN_LODE=160
FACE_REG_SIZE = 150
FACE_REG_SIZE_Y = 350
REG_BOX_BORDER_X = 150
REG_BOX_BORDER_Y = 350
REG_BOX_ADJUST_X_BLUE = 50
SCREEN_RESOLUTION_WIDTH = 1200  # 1590 #1600 #1200 #1600 #1280 #800
SCREEN_RESOLUTION_HEIGHT = 710  # 880 #710 #880 #710 #480
REG_BOX_POS_X = SCREEN_RESOLUTION_HEIGHT/2
REG_BOX_POS_Y = SCREEN_RESOLUTION_WIDTH/2
reg_box_x1 = REG_BOX_POS_X - (FACE_REG_SIZE / 2) - REG_BOX_BORDER_X - REG_BOX_ADJUST_X_BLUE
reg_box_x2 = REG_BOX_POS_X + (FACE_REG_SIZE / 2) + REG_BOX_BORDER_X + REG_BOX_ADJUST_X_BLUE
reg_box_y1 = REG_BOX_POS_Y - (FACE_REG_SIZE_Y / 2) - REG_BOX_BORDER_Y#- REG_BOX_ADJUST_Y1_BLUE
reg_box_y2 = REG_BOX_POS_Y + (FACE_REG_SIZE_Y / 2) + REG_BOX_BORDER_Y
#=============Global Variable End=========================
#=============Function Start==============================
def rotate_img(mat, angle):
    # angle in degrees
    height, width = mat.shape[:2]
    image_center = (width / 2, height / 2)

    rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

    abs_cos = abs(rotation_mat[0, 0])
    abs_sin = abs(rotation_mat[0, 1])

    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    rotation_mat[0, 2] += bound_w / 2 - image_center[0]
    rotation_mat[1, 2] += bound_h / 2 - image_center[1]

    rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))
    return rotated_mat

def drawText(cv_img, txt, pos,color,font_size=44):
    # img = Image.fromarray(cv_img)
    img = cv_img
    draw = ImageDraw.Draw(img)
    try:
        draw.text(pos, unicode(txt, "utf-8"),  color, font=ImageFont.truetype(CHINESE_FONT, font_size))
    except IOError as err:
        pass
        # print err
        # print('No Font File found')
        # main_logger.info("font err:"+str(err))
    # img = np.array(img)
    return img

def drawDateTime(img,top_img,serialNumber):
    img1=img.copy()
    img=cv2.addWeighted(img[0:top_img.shape[0], 0:top_img.shape[1]], 0.6, top_img, 0.4, 4)
    hktime=time.strftime("%Y年%m月%d日%H:%M", time.localtime(time.time()))
    img = drawText(img, "機器編號："+serialNumber+"                        "+hktime, (20, 5),(255,255,255), 22)
    img = drawText(img,"          ●網絡已連接                                                      CIC已連接", (20, 40),(0,255,0), 22)
    img[reg_box_y1:reg_box_y2-DOWN_LODE,reg_box_x1:reg_box_x2]=img1[reg_box_y1:reg_box_y2-DOWN_LODE,reg_box_x1:reg_box_x2]
    img = cv2.rectangle(img, (reg_box_x1, reg_box_y1), (reg_box_x2, reg_box_y2 - DOWN_LODE), (0, 255, 0), 2)
    return img

def startUpScreen():

    startFrame = cv2.imread(START_PGN)
    startFrame = Image.fromarray(startFrame)
    startFrame1 = drawText(startFrame, START_VI,
                                (SCREEN_RESOLUTION_HEIGHT / 2 - 60, SCREEN_RESOLUTION_WIDTH - 80), (0, 0, 0), 18)
    startFrame1 = np.array(startFrame1)
    startFrame2 = drawText(startFrame, "啟動中..",
                                (SCREEN_RESOLUTION_HEIGHT / 2 - 80, SCREEN_RESOLUTION_WIDTH / 2 + 60), (63, 85, 14),
                                50)
    startFrame2 = np.array(startFrame2)
    startFrame1 = rotate_img(startFrame1, 90)
    cv2.imshow('start', startFrame1)
    cv2.moveWindow('start', 0, 0)
    cv2.waitKey(1000)
    subprocess.call('aplay /opt/main/remoteFirefly/pbox/sound/sound-welcome.wav &', shell=True)
    startFrame2 = rotate_img(startFrame2, 90)
    cv2.imshow('start', startFrame2)
    cv2.waitKey(1000)
    cv2.destroyAllWindows()

# def drawTickAndCross(img, img_logo):
#     img2 = Image.fromarray(img_logo)
#     # img2.show()
#     img = Image.fromarray(img)
#     w, h = img2.size
#     i_w, i_h = img.size
#     # print i_w, i_h, w, h
#     # box = (i_w - w, i_h - h, i_w, i_h)
#     # print box
#     box = ((i_w) / 2 - w / 2, i_h - h - 250, i_w / 2 + w / 2, i_h - 250)
#     # box=()
#     # print box
#     mosaic = img2.convert('RGBA')
#     layer = Image.new('RGBA', img.size, (0, 0, 0, 0))
#     layer.paste(mosaic, box)
#     xxx = Image.composite(layer, img, layer)
#     xxx = xxx.convert('RGB')
#     array1 = np.array(xxx)
#     # array1 = cv2.cvtColor(array1, cv2.COLOR_RGB2BGR)
#     # cv2.imshow("xxxxx",array1)
#     # cv2.waitKey(60)
#     return array1

# def drawDoubleTickAndCross(img, img_logo):
#     img2 = Image.fromarray(img_logo)
#
#     img = Image.fromarray(img)
#     w, h = img2.size
#     i_w, i_h = img.size
#
#     box = ((i_w) / 2 - w-10, i_h - h - 250, i_w / 2-10, i_h - 250)
#     box2= ((i_w) / 2 +10, i_h - h - 250, i_w / 2+w+10, i_h - 250)
#
#     mosaic = img2.convert('RGBA')
#     layer = Image.new('RGBA', img.size, (0, 0, 0, 0))
#     layer.paste(mosaic, box)
#
#     layer2 = Image.new('RGBA', img.size, (0, 0, 0, 0))
#     layer2.paste(mosaic, box)
#     layer2.paste(mosaic, box2)
#
#     xxx = Image.composite(layer, img, layer)
#     xxx = xxx.convert('RGB')
#     array1 = np.array(xxx)
#
#     xxx = Image.composite(layer2, img, layer2)
#     xxx = xxx.convert('RGB')
#     array1 = np.array(xxx)
#
#     return array1

def drawTickOrCross(img,tickOrCross):
    drawImg = img
    w, h = tickOrCross.size
    i_w, i_h = img.size
    box = ((i_w) / 2 - w / 2, i_h - h - 250, i_w / 2 + w / 2, i_h - 250)
    drawImg.paste(tickOrCross, box)
    return drawImg

def drawDoubleTickOrCross(img,tickOrCross):

    drawImg = img
    w, h = tickOrCross.size
    i_w, i_h = img.size

    box = ((i_w) / 2 - w - 10, i_h - h - 250, i_w / 2 - 10, i_h - 250)
    box2 = ((i_w) / 2 + 10, i_h - h - 250, i_w / 2 + w + 10, i_h - 250)

    # mosaic = img2.convert('RGBA')
    # layer = Image.new('RGBA', img.size, (0, 0, 0, 0))
    drawImg.paste(tickOrCross, box)
    drawImg.paste(tickOrCross, box2)

    # layer2 = Image.new('RGBA', img.size, (0, 0, 0, 0))
    # layer2.paste(mosaic, box)
    # layer2.paste(mosaic, box2)
    #
    # xxx = Image.composite(layer, img, layer)
    # xxx = xxx.convert('RGB')
    # array1 = np.array(xxx)
    #
    # xxx = Image.composite(layer2, img, layer2)
    # xxx = xxx.convert('RGB')
    # array1 = np.array(xxx)

    return drawImg


#=============Function End================================

#=============Class Start=================================
class display:

    def __init__(self,serialNumber,videoCap, server):
        self.serialNumber = serialNumber
        self.cap = videoCap
        self.serverSocket = server
        self.cross = Image.open(ERROR_MODE).convert('RGBA')
        self.tick = Image.open(SUCCESS_SMALL_MODE).convert('RGBA')
        self.base = []
        self.face = []
        self.smallFace = []
        self.debug = ''
        self.score = ''
        self.state = ''
        self.workerID = ''
        self.stopped = False
        # self.img = []
        self.passScore = 0
        self.faceStart = 0
        # self.screenCap = False

    def start(self):
        threading.Thread(target=self.drawFrame, args=()).start()
        threading.Thread(target=self.displayFrame, args=()).start()
        return self

    def stop(self):
        self.stopped = True

    def drawFrame(self):
        tagStartTime = time.time()
        count = 0
        startTime = time.time()
        while not self.stopped:
            base = Image.open('./img/background3.png').convert('RGBA')
            # draw = ImageDraw.Draw(base)
            hktime = time.strftime("%Y年%m月%d日%H:%M", time.localtime(time.time()))
            base = drawText(base, "機器編號：" + self.serialNumber + "                        " + hktime, (20, 5), (255, 255, 255),22)

            if self.serverSocket.serverState == 0:
                base = drawText(base, "          ●網絡已連接                                                      CIC已連接",(20, 40), (0, 255, 0), 22)
            else:
                base = drawText(base, "          ●網絡已連接                                                      CIC已連接",(20, 40), (255, 0, 0), 22)

            if self.face != []:
                ims = self.face
                ims = Image.fromarray(ims)
                # ims = cv2.resize(self.face, (240, 300))
                # base[400:700, 0:240] = ims
                w, h = ims.size
                # ims.save("./img/pic3.png", "PNG")
                base.paste(ims, (0, 400, w, 400+h))
                ImageDraw.Draw(base).rectangle((0, 400, w, 400+h), fill=None, outline=(0, 255, 0))
                # base = ImageDraw(base)
                # base.
                # base.paste(ims, (0,400,240,700))
                # draw.rectangle([(0, 400), (240, 700)], (0, 255, 0))
                faceTimeOut = time.time() - self.faceStart
                if faceTimeOut > 1: # Set pic dispaly time
                    self.face = []
                    self.faceStart = 0
                    # if count == 0:
                    #     self.screenCap = True
                    #     count = count+1

            if self.smallFace != []:
                ims1 = self.smallFace
                ims1 = Image.fromarray(ims1)
                w, h = ims1.size
                base.paste(ims1, (0, 0, w, h))
                # base.paste(ims1, (0, 0, 128, 128))
                smallFaceTimeOut = time.time() - self.smallFaceStart
                if self.score != '':
                    base = drawText(base, str(self.score),(10,1100), (255, 0, 0), 50)
                if smallFaceTimeOut > 1:  # Set pic dispaly time
                    self.smallFace = []
                    self.score = ''
                    self.smallFaceStart = 0

            if self.workerID != '':
                base = drawText( base, "ID:" + self.workerID, ((SCREEN_RESOLUTION_HEIGHT / 2 - 5 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE + 50), (255, 255, 255), 50)
                if self.state == 'working':
                    base = drawText(base, "識別中..",((SCREEN_RESOLUTION_HEIGHT / 2 - 3 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE - 50), (255, 255, 255), 50)
                elif self.state == 'pass':
                    base = drawText(base, "可以通行", ((SCREEN_RESOLUTION_HEIGHT / 2 - 4 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE - 50), (0, 255, 0), 50)
                    base = drawDoubleTickOrCross(base, self.tick)
                    if self.debug == 'sp':
                        base = drawText(base, "Server Pass", (150, 100), (0, 255, 0),50)
                        base = drawText(base, str(self.passScore), (150, 150), (0, 255, 0), 50)
                    elif self.debug == 'lp':
                        base = drawText(base, "local Pass", (150, 100), (0, 255, 0), 50)
                        base = drawText(base, str(self.passScore), (150, 150), (0, 255, 0), 50)
                    else:
                        pass
                elif self.state == 'suspect':
                    base = drawText(base, "可以通行", ((SCREEN_RESOLUTION_HEIGHT / 2 - 4 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE - 50),(0, 255, 0), 50)
                    base = drawTickOrCross(base, self.tick)
                    base = drawText(base, "Server Pass", (150, 100), (0, 255, 0), 50)
                    base = drawText(base, str(self.passScore), (150, 150), (0, 255, 0), 50)
                elif self.state == 'fault':
                    base = drawText(base, "不可通過", ((SCREEN_RESOLUTION_HEIGHT / 2 - 4 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE - 50), (255, 0, 0), 50)
                    base = drawTickOrCross(base, self.cross)
                    if self.debug == 'timeOut':
                        base = drawText(base, "Time Out", (150, 100), (255, 0, 0), 50)
                elif self.state == 'temp':
                    base = drawText(base, "臨時卡",((SCREEN_RESOLUTION_HEIGHT / 2 - 4 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE - 50),(0, 255, 0), 50)
                    base = drawTickOrCross(base, self.tick)
                elif self.state == 'norecord':
                    base = drawText(base, "未登記", ((SCREEN_RESOLUTION_HEIGHT / 2 - 4 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE - 50), (255, 0, 0), 50)
                    base = drawTickOrCross(base, self.cross)
                elif self.state == 'greenCardExpiry':
                    ase = drawText(base, "平安卡過期",
                                   ((SCREEN_RESOLUTION_HEIGHT / 2 - 4 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE - 50),
                                   (255, 0, 0), 50)
                    base = drawTickOrCross(base, self.cross)
                else:
                    pass
            else:
                if time.time() - tagStartTime > 1:
                    tagStartTime = time.time()
                else:
                    base = drawText(base, "請拍卡", ((SCREEN_RESOLUTION_HEIGHT / 2 - 3 * 25), SCREEN_RESOLUTION_WIDTH - DOWN_LODE - 50), (255, 255, 255), 50)

            self.base = base.transpose(Image.ROTATE_90)
            count +=1
        else:
            stopTime = time.time()
            totalTime = stopTime - startTime
            print "[DM] draw frame rate: ",count/totalTime, totalTime, count

    def displayFrame(self):
        time.sleep(2)
        print "[DM] =====Display start====="
        count = 0
        startTime = time.time()
        while not self.stopped:
            frame = self.cap.read()
            frame = cv2.flip(frame, 1)
            frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGBA)
            frame = cv2.resize(frame,(1200,710))
            img = Image.fromarray(frame)
            img = Image.alpha_composite(img, self.base)
            out = img.convert('RGB')
            out = np.array(out)
            imshow = cv2.cvtColor(out, cv2.COLOR_RGB2BGR)
            cv2.imshow('CiSystem', imshow)
            cv2.moveWindow('CiSystem', 0, 0)
            cv2.waitKey(1)
            count += 1
        else:
            stopTime = time.time()
            totalTime = stopTime - startTime
            print "[DM] display frame rate: ", count/totalTime, totalTime, count
            cv2.destroyAllWindows()

    def set(self,name,value):
        if name == 'workerID':
            self.workerID = value
        elif name == 'state':
            self.state = value
        elif name == 'face':
            self.face = value
            self.faceStart = time.time()
        elif name == 'smallFace':
            self.smallFace = value
            self.smallFaceStart = time.time()
        elif name == 'score':
            self.score = value
        elif name == 'cap':
            self.screenCap = value
        elif name == 'debug':
            self.debug = value
        elif name == 'passScore':
            self.passScore = value
        else:
            pass

    def clear(self):
        self.passScore = 0
        self.state = ''
        self.workerID = ''
        self.debug = ''

    # def get_img(self):
    #     imgCut = self.img[reg_box_y1:reg_box_y2 - DOWN_LODE, reg_box_x1:reg_box_x2]
    #     return imgCut

if __name__ == "__main__":

    # startUpScreen()
    videoCap = WebcamVideoStream(src=0)
    videoCap.start()
    screen = display(SERIAL_NUMBER, videoCap)
    screen.start()

    starttime = time.time()
    face = Image.open('./img/test.png').convert('RGBA')
    face = face.resize((240, 300))
    while 1:
        stoptime = time.time() - starttime
        if stoptime > 60:
            break
        elif stoptime > 50:
            screen.setAtr('face', '')
        elif stoptime > 45:
            screen.setAtr('workerID', '')
            screen.setAtr('face', face)
        elif stoptime > 40:
            screen.setAtr('state', 'suspect')
        elif stoptime > 35:
            screen.setAtr('state', 'norecord')
        elif stoptime > 30:
            screen.setAtr('state', 'fault')
        elif stoptime > 25:
            screen.setAtr('state', 'pass')
        elif stoptime > 10:
            screen.setAtr('workerID', 'CWR12345678')
            screen.setAtr('state', 'working')
        else:
            pass
        time.sleep(1)
    screen.stop()

#=============Class End===================================
