# coding:utf-8
import os
import pika
import time
import json
import base64
import shutil
import threading
import subprocess
import ConfigParser

class mqController:
    # def __init__(self,username,password,ip,port,vhost,siteID,serialNumber, cideviceFace,pboxConfig):
    def __init__(self, pboxConfig, list, server):
        self.config = pboxConfig
        self.list = list
        self.siteID = pboxConfig.serial_siteID
        self.serialNumber = pboxConfig.serial_number
        self.localFaceDir = pboxConfig.pbox_localFaceDir
        self.cicConnected = pboxConfig.pbox_cicConnected
        self.username = pboxConfig.server_mq_username
        self.password = pboxConfig.server_mq_password
        self.ip = pboxConfig.server_mq_ip
        self.port = pboxConfig.server_mq_port
        self.vhost = pboxConfig.server_mq_vhost
        self.saveDevice = pboxConfig.server_mq_saveDevice
        self.saveImage = pboxConfig.server_mq_saveImg
        self.serverState = server.serverState
        self.serverResume = server.serverResume

    def start(self):
        threading.Thread(target=self.run, args=()).start()
        return

    def stop(self):
        self.stopped = True

    def run(self):
        while 1:
            try:
                # print server_username, server_password
                credentials = pika.PlainCredentials(self.username, self.password)
                parameters = pika.ConnectionParameters(self.ip, self.port, self.vhost, credentials)
                parameters.socket_timeout = 1
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                siteID = self.siteID
                queueName = self.serialNumber
                # 如果生产者没有运行创建队列，那么消费者创建队列
                channel.queue_declare(queue=queueName, durable=True)
                # 声明交换机类型
                channel.exchange_declare(exchange=siteID,
                                         type='fanout', durable=True)
                channel.basic_consume(self.callback,
                                      queue=queueName,
                                      no_ack=False)
                channel.queue_bind(exchange=siteID,
                                   queue=queueName)
                # print('[MQ] [*] Waiting for messages. To exit press CTRL+C')
                print '[MQ] RabbitMQ consumer Ready'
                channel.start_consuming()
                print '[MQ] RabbitMQ lose connection'
            except Exception as err:
                self.serverResume = 1
                print'[MQ] rabbitMQ resume err'
                print err
                time.sleep(5)
            time.sleep(1)

    def callback(self,ch, method, properties, body):
        print("[MQ] [x] Received %r" % body)
        while 1:
            try:
                print "[MQ] no loads"
                print "[MQ] ", type(body)
                x = json.loads(body)
            except:
                print "[MQ] loads"
                x = body
            print "[MQ] ", type(x)
            print "[MQ] ", x
            try:
                if str(x['mode']) == 'delete':
                    workerID = str(x['cwr_number'])
                    # os.popen("sudo rm -rfcwr_number "+str(self.localFaceDir)+delectx )

                    #Delete worker face saving directory
                    if len(workerID) > 7:
                        faceDir = str("3" + workerID[-7:])
                    print faceDir
                    try:
                        shutil.rmtree(self.localFaceDir + faceDir)
                    except Exception as err:
                        print err
                        pass

                    #Delete worker in the list
                    # try:
                    #     if workerID in self.list.CWRCard:
                    #         CWRIndex = self.list.CWRCard.index(workerID)
                    #         serialNumber = self.list.CWRCard[CWRIndex + 1]
                    #         if serialNumber[:3] != "CWR":
                    #             del self.list.CWRCard[CWRIndex + 1]
                    #             del self.list.serialMatching[str(serialNumber)]
                    #             self.list.workerSave.remove_option("serialMatching", str(serialNumber))
                    #         del self.list.CWRCard[CWRIndex]
                    #         self.list.workerSave.remove_option("allWorker", "cwr_card")
                    #         self.list.workerSave.set("allWorker", "cwr_card", value=str(",".join(self.list.CWRCard)))
                    #     elif workerID in self.list.whiteCard:
                    #         self.list.whiteCard.remove(str(workerID))
                    #         self.list.workerSave.remove_option("allWorker", "white_card")
                    #         self.list.workerSave.set("allWorker", "white_card", value=str(",".join(self.list.whiteCard)))
                    #     elif workerID in self.list.tempCard:
                    #         self.list.tempCard.remove(str(workerID))
                    #         self.list.workerSave.remove_option("allWorker", "temp_card")
                    #         self.list.workerSave.set("allWorker", "white_card", value=str(",".join(self.list.tempCard)))
                    #     else:
                    #         pass
                    # except:
                    #     pass
                    #
                    # self.list.workerSave.write(open("worker_list.ini", "w"))

                    print "delete over"
                    # try:
                    #     mqsave("?siteID=" + str(Site_id)+"&employeeID=" + str(delectx), del_employeeImages)
                  # except:
                    #     print "m"
                elif str(x['mode']) == "install":
                    now = time.time()
                    workerID = x['cwr_number']
                    imgdata = base64.b64decode(x['minimg'])
                    try:
                        os.mkdir(self.localFaceDir + str(workerID))
                    except:
                        print"no mkdir" + str(workerID)
                        pass
                    filex = open(self.localFaceDir + str(workerID) + "/" + str(now) + ".dat", 'wb')
                    filex.write(imgdata)
                    filex.close()
                    print workerID
                    print "x show serv"
                    # install_mq = {
                    #     "employeeID": str(cwr_number),
                    #     "imageNames": [str(self.serialNumber) + "_pic_" + str(now)],
                    #     "serial_number": str(self.serialNumber),
                    #     "siteID": self.Site_id
                    # }
                    # install_mq = json.dumps(install_mq)
                    # try:
                    #     print "mqsavedevice success "
                    #     self.mqsave(install_mq, self.mqsavedevice)
                    # except:
                    #     print "mqsavedevice error"
                    #     pass
                    # install_mqqq = {
                    #     "imageNames": (str(self.serialNumber) + "_pic_" + str(now)),
                    #     "data": x['minimg']
                    # }
                    # install_mqqqq = json.dumps(install_mqqq)
                    # try:
                    #     self.mqsave(install_mqqqq, self.mqsave_image)
                    # except:
                    #     print "mqsave error"
                    #     pass

                    # if len(serial_numbers) > 8:
                    #     serial_numbers = '3' + serial_numbers[len(serial_numbers) - 7:len(serial_numbers)]
                    # for i in os.listdir(OUTPUT_DIRECTORY):
                    #     if i==serial_numbers:
                    #         shutil.rmtree(OUTPUT_DIRECTORY +serial_numbers)
                    print "run minimg"
                elif str(x['mode']) == "update":
                    if self.cicConnected == 1:
                        try:
                            subprocess.call('./get_goodlist.sh', shell=True)
                            # main_logger.info('./get_goodlist.sh')
                        except Exception as err:
                            pass
                            # main_logger.info('./get_goodlist.sh err' + str(err))
                    print "update"
                    # while not self.list.update():
                    #     time.sleep(3)
                elif str(x['mode']) == "updateDevice":
                    print "updateDevice"
                    print str(x['Serial_Number'])
                    print self.serialNumber
                    if str(x['Serial_Number']) == self.serialNumber:
                        print "update Device"
                        time.sleep(2)
                        ch.basic_ack(delivery_tag=method.delivery_tag)
                        siteUpdateSuccess = False
                        while not siteUpdateSuccess:
                            siteUpdateSuccess = self.config.update_site()
                            # try:
                            #     print MGDB + "/GetdeviceBy_deviceID?Serial_Number=" + str(self.serialNumber)
                            #     Site_r = requests.get(
                            #         MGDB + "/GetdeviceBy_deviceID?Serial_Number=" + str(self.serialNumber))
                            #     # print Site_r.text
                            #     Site_r = Site_r.json()
                            #     Site_id = Site_r["Site_id"]
                            #     Serial_Number_ID = Site_r["_id"]
                            #     Company_id = Site_r["Company_id"]
                            #     Position = Site_r["Position"]
                            #     ChkGreenExp = Site_r["chkGreenExp"]
                            #     print Site_id
                            #
                            #     sp.set('inset', 'chkGreenExp', value=str(ChkGreenExp))
                            #     sp.set('inset', 'SITEID', value=str(Site_id))
                            #     sp.set('inset', 'Serial_Number_ID', value=str(Serial_Number_ID))
                            #     sp.set('inset', 'Company_id', value=str(Company_id))
                            #     sp.set('inset', 'Position', value=Site_r["Position"])  # str(Position.encode('UTF-8')))
                            #     # DEVICEID_ID = lp.get('facial', 'DEVICEID_ID')
                            #     print "serial_number"
                            #     sp.write(open("./pbox_config/serial_number.ini", "w"))
                            #     print sp.get('inset', 'SITEID')
                            #     # main_logger.info("use_server_Pbox")
                            #     return
                            # except Exception as err:
                            #     print err
                            #     # main_logger.info("can't update logic site")

                    else:
                        ch.basic_ack(delivery_tag=method.delivery_tag)
                        return
                elif str(x['mode']) == "updateSite":
                    self.config.updateSite
                        # time.sleep(2)
                        # ch.basic_ack(delivery_tag=method.delivery_tag)
                        # while 1:
                        #     try:
                        #         # print MGDB + "/GetdeviceBy_deviceID?Serial_Number=" + str(Serial_Number)
                        #         # Site_r = requests.get(
                        #         #     MGDB + "/GetdeviceBy_deviceID?Serial_Number=" + str(Serial_Number))
                        #         # # print Site_r.text
                        #         # Site_r = Site_r.json()
                        #         # Site_id = Site_r["Site_id"]
                        #         # Serial_Number_ID = Site_r["_id"]
                        #         # Company_id = Site_r["Company_id"]
                        #         # Position = Site_r["Position"]
                        #         # ChkGreenExp = Site_r["chkGreenExp"]
                        #         # print Site_id
                        #         #
                        #         # sp.set('inset', 'chkGreenExp', value=str(ChkGreenExp))
                        #         # sp.set('inset', 'SITEID', value=str(Site_id))
                        #         # sp.set('inset', 'Serial_Number_ID', value=str(Serial_Number_ID))
                        #         # sp.set('inset', 'Company_id', value=str(Company_id))
                        #         # sp.set('inset', 'Position', value=Site_r["Position"])  # str(Position.encode('UTF-8')))
                        #         # # DEVICEID_ID = lp.get('facial', 'DEVICEID_ID')
                        #         # print "serial_number"
                        #         # sp.write(open("./pbox_config/serial_number.ini", "w"))
                        #         # print sp.get('inset', 'SITEID')
                        #         # main_logger.info("use_server_Pbox")
                        #         # if ChkGreenExp == "Y":
                        #         #     print "Y"
                        #         #     start_greey = 1
                        #         # else:
                        #         #     start_greey = 0
                        #         # return
                        #     except Exception as err:
                        #         print err
                        #         main_logger.info("can't update logic site")
                print "mq over"
                time.sleep(2)
                ch.basic_ack(delivery_tag=method.delivery_tag) # 主要使用此代码
                self.list.update()
                break
            except Exception as err:
                print "other error "
                print err
                time.sleep(5)
                pass

    @staticmethod
    def mqsave(message, queueName):
        params = pika.URLParameters('amqp://yuanwendong:123456@localhost/%2f')
        params.socket_timeout = 1
        connection = pika.BlockingConnection(params)
        channel = connection.channel()
        channel.queue_declare(queue=queueName, durable=True)  # 创建一个新队列task_queue，设置队列持久化，注意不要跟已存在的队列重名，否则有报错
        print "[MQ] send MQ"
        channel.basic_publish(exchange='', routing_key=queueName,  # 写明将消息发送给队列worker
                              body=message,  # 要发送的消息
                              properties=pika.BasicProperties(delivery_mode=2, )  # 设置消息持久化，将要发送的消息的属性标记为2，表示该消息要持久化
                              )

if __name__ == "__main__":

    pboxConfig = ConfigParser.SafeConfigParser()
    pboxConfig.read('./pbox_config/pbox.ini')
    local_like_score = pboxConfig.get('local', 'local_like_score')
    messageQueue = mqController(pboxConfig)
    local_like_score_2 = pboxConfig.get('local', 'local_like_score')
    pass