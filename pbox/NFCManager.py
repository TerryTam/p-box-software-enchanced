# coding:utf-8
import cv2
import json
import gc
from PIL import Image
from cStringIO import StringIO
import time
import subprocess
import threading
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

nfcID = ''

class nfcHandler(BaseHTTPRequestHandler):
    # input received from nfc
    # def __init__(self, list):
    #     self.list = list

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        # global card_info
        # global last_card
        # global HTTPREUTRN
        # global pro_mode
        # global photo_count
        # global worker_card_id
        # global buf_worker_card_id
        # global worker_id_input
        # global success_reg_count
        # global total_reg_count
        # global status
        # global no_of_jury_passed
        # global reset_worker_mode
        # global jury
        # global work_mode
        # global recognized_photo
        # global WORKER_MODE
        # global ADMIN_MODE
        # global preload
        # global admin_mode_stage
        # global globalRstarted
        # global incorrect_card
        # global total_detected_face_for_reg
        # global buffered
        # global worker_NFC
        # global update_worker_id
        global nfcID
        self._set_headers()
        # self.wfile.write("<html><body>Waiting for NFC</body></html>" + self.path)
        nfcData = str(self.path.replace("/nfc?id=", ""))
        action = self.path[:4]
        # if time.time()-card_info>1 or worker_id_from_NFC!=last_card :
        #     pass
        # else:
        #     card_info=time.time()
        #     return
        # if worker_id_input!="":
        #     return

        # subprocess.call('aplay /opt/main/sound/card.wav', shell=True)
        print "[NFC] NFC ID: ",nfcData
        # if len(worker_id_from_NFC) > 8:
        #     worker_id_from_NFC = '3' + worker_id_from_NFC[len(worker_id_from_NFC) - 7:len(worker_id_from_NFC)]
        #     print(worker_id_from_NFC)
        #     print "id cade"
        # print pro_mode == WORKER_MODE and worker_id_from_NFC == "36723506"
        # if worker_id_from_NFC == "32962316" and action == "/nfc":
        #     if work_mode == ADMIN_MODE:
        #         work_mode = WORKER_MODE
        #     elif work_mode == WORKER_MODE:
        #         work_mode = ADMIN_MODE
        # elif worker_id_from_NFC != "" and action == "/nfc":
        #     last_card=worker_id_from_NFC
        #     mylock.acquire()
        #     worker_NFC = worker_id_from_NFC
        #     mylock.release()
        # time.sleep(6)
        if nfcData != "" and action == "/nfc":
            # if nfcData[:3] != "CWR":
            #     if nfcData in self.list.CWRCard:
            #         nfcData = self.list.serialMatching[str(nfcData)]
            nfcID = str(nfcData)


        # Result="0"
        # if HTTPREUTRN:
        #     Result="1"

        # data={
        #     "CWRNo": str(worker_id_from_NFC),
        #     "Result": Result,
        #     "Datetime": str(time.strftime('%Y%m%d%H%M%S'))
        # }
        # j_str = json.dumps(data)
        # self.wfile.write(j_str)

    def do_POST(self):
        global worker_NFC
        print "[NFC] have request"
        self._set_headers()
        datas = self.rfile.read(int(self.headers['content-length']))


        print "[NFC] ", datas
        print "[NFC] ", type(datas)
        worker_id_from_NFC = datas[7:-2]
        worker_NFC = worker_id_from_NFC
        # print type(xxx)
        self.send_response(200)

        self.send_header("Welcome", "Contect")
        self.wfile.write(1)
        self.wfile.close()
        return ""

class nfcListener:

    global nfcID

    def __init__(self):
        self.stopped = 0
        # self.list = list

    def start(self):
        threading.Thread(target=self.run, args=()).start()
        return self

    def stop(self):
        # self.t1.join()
        self.stopped = 1

    def run(self):
        # global handle_httpd_request
        # time.sleep(0.001)
        # handler = nfcHandler(self.list)
        httpd = HTTPServer(('', 8081), nfcHandler)
        # print "install nfc"
        # self.httpd.serve_forever()
        print "[NFC] =====NFC listener start====="
        while not self.stopped:
                httpd.handle_request()
                time.sleep(0.1)
        #     self.httpd.server_close()
        #     break
            # if not self.nfcstop:
            #     self.httpd.handle_request()
            #     time.sleep(0.5)
        # else:
        print "[NFC] End of nfc Listener"
            # print self.Stop
            #
            # while handle_httpd_request == True:
            #     self.httpd.handle_request()
            #     print "install nfc"
            #     if handle_httpd_request == False:
            #         break

    # def stop(self):
    #     self.nfcstop = True
        # print self.nfcListenerStop
        # self.httpd.server_close()
        # self.httpd.shutdown()
        # try:  # ignore the stupid errors.
        #     # self.httpd.socket.close()
        #     self.nfcListenerStop = True
        #     self.httpd.stop()
        #     self.httpd.shutdown()
        #
        # except:
        #     pass
    #
    # def __del__(self):
    #     self.httpd.shutdown()
    #     self.httpd.stop()
    #     self.stop
    def get_ID(self):
        global nfcID
        return nfcID

    def clear_ID(self):
        global nfcID
        nfcID = ''


if __name__ == "__main__":
    nfc = nfcListener()
    nfc.start()
    startTime = time.time()
    while 1:
        if time.time() - startTime > 3:
            nfc.stop()
            time.sleep(10)
            break


    print "Program Ended"

