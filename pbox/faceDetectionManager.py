
import os
import cv2
import dlib
import time
import requests
from cStringIO import StringIO
import thread
import threading
import numpy as np
from PIL import Image
from imutils.video import WebcamVideoStream

LOG_IMG_FOLDER = "./log_img/"

FaceRecognizer=[]
registeredWorkerID=[]
one_registeredWorkerID=[]
handicappedFaceRecognizer=[]
MAX_REG_CONF_REJECT =200
CAM_RESOLUTION_WIDTH = 1200  # 1590 #1600 #1200 #1600 #1280 #800
CAM_RESOLUTION_HEIGHT = 710  # 880 #710 #880 #710 #480
# mylock = thread.allocate_lock()

FaceDetector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
FacePredictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
face_gleass = cv2.CascadeClassifier("haarcascade_mcs_lefteye.xml")
Detector= cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#perform r1,now don't hava perform r2

def opencv_base64(img):
    try:
        im = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    except:
        im=img
    imImage = Image.fromarray(im)
    # imImage = cv2.cvtColor(imImage, cv2.COLOR_RGB2BGR)
    output = StringIO()
    imImage.save(output, format='JPEG')
    im_data = output.getvalue()
    payload1 = im_data.encode("base64")
    return payload1

def rotate_img(mat, angle):
    # angle in degrees
    height, width = mat.shape[:2]
    image_center = (width / 2, height / 2)

    rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

    abs_cos = abs(rotation_mat[0, 0])
    abs_sin = abs(rotation_mat[0, 1])

    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    rotation_mat[0, 2] += bound_w / 2 - image_center[0]
    rotation_mat[1, 2] += bound_h / 2 - image_center[1]

    rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))
    return rotated_mat

def faceDetectForRecognition(_img):

    global faceOnScreen
    global haveFace

    global ImageX
    global ImageY
    global ImageW
    global ImageH

    # hhh, www = _img.shape[:2]
    # print hhh,www
    # 1184, 620
    # 656 1184

    ImageT = 0
    try:
        _img=cv2.cvtColor(_img, cv2.COLOR_BGR2GRAY)
    except:
        _img=_img
    # print "[FD] ", _img.shape[0],_img.shape[1]
    camA = 0
    camB = _img.shape[0]
    camC = 0
    camD = _img.shape[1] - 0
    # print "[FD] ", int(camA),int(camB),int(camC),int(camD)
    # print "[FD] ", _img.shape[0],_img.shape[1]
    img = _img[
          (int)(camA):(int)(camB),
          (int)(camC):(int)(camD)
          ]
    # img=_img
    imgMaxY = _img.shape[0]
    imgMaxX = _img.shape[1]
    # print "[FD] ", imgMaxX,imgMaxY
    grayX = img
    # cv2.imshow("testttt",_img)
    # cv2.waitKey(600)
    facesX = Detector.detectMultiScale(grayX, 1.01, 7, 0, minSize=(130, 130), maxSize=(imgMaxY, imgMaxX))
    # print "[FD] face detected"
    # print "[FD] ", facesX
    # print "[FD] ", len(facesX)
    if len(facesX) < 1:
        print "[FD] No face found "
        resized_face = []
        return resized_face
    resized_face = []
    faceOnScreen = False
    haveFace = 0

    dh = 0
    dw = 0
    ImageT = 0
    ImageX = 0
    ImageY = 0
    ImageW = 0
    ImageH = 0

    maxHeight = 0
    maxArea = 0

    for (x, y, w, h) in facesX:

        # print "[FD] face:" + str(w) + "," + str(h)
        # print "[FD] number of faces detected: ",str(len(facesX))
        if (((w * h) > maxArea) or (((w * h) == maxArea) and (h > maxHeight))):
            maxHeight = h
            maxArea = w * h
            dh = 0
            dw = 0
            ImageT = 1
            ImageX = x + dw
            ImageY = y - dh
            ImageW = w - dw * 2
            ImageH = h + dh

    # second cut to try get a better rectangle


    if ImageT > 0:
        #		imgMaxY2 = (int)((imgMaxY+ImageH)/2)
        imgMaxY2 = (int)((imgMaxY - (ImageY / 16)))  # 3
        imgMaxX2 = (int)((imgMaxX + ImageW) / 2)
        ImageY2 = (int)(ImageY / 2)
        ImageX2 = (int)(ImageX / 2)
        secondCut = grayX[
                    ImageY2:imgMaxY2,
                    ImageX2:imgMaxX2
                    ]

        # print ImageY2,imgMaxY2,ImageX2,imgMaxX2

        facesX = Detector.detectMultiScale(secondCut, 1.01, 7, 0, minSize=(100, 100), maxSize=(imgMaxY2, imgMaxX2))

        ImageT = 0  # 1
        ImageX = 0  # x+dw
        ImageY = 0  # y-dh
        ImageW = 0  # w-dw*2
        ImageH = 0  # h+dh

        maxHeight = 0
        maxArea = 0

        for (x, y, w, h) in facesX:

            # print("face")
            # print ("number of faces detected : ",str(len(facesX)))
            if (((w * h) > maxArea) or (((w * h) == maxArea) and (h > maxHeight))):
                maxHeight = h
                maxArea = w * h
                dh = 0  # 60
                dw = 0
                ImageT = 1
                ImageX = x + ImageX2
                ImageY = y + ImageY2
                ImageW = w
                ImageH = h

    # print str(ImageT)

    if ImageT > 0:

        global leftEyeX
        global leftEyeY
        global rightEyeX
        global rightEyeY
        global noseTipX
        global noseTipY
        global mouthX
        global mouthY
        global eyeDistance
        global eyeNoseDistance
        global headLX
        global headLY
        global headRX
        global headRY
        global imgCrop
        global right_eye_side_distance
        global left_eye_side_distance

        noseTipX = -1
        leftEyeY = -1
        rightEyeY = -1
        mouthY = -1

        haveFace = 1  # show up rectangle on screen
        box = dlib.rectangle(left=ImageX, top=ImageY, right=ImageX + ImageW, bottom=ImageY + ImageH)

        shape = []
        shape = FacePredictor(img, box)

        leftEyeX = ((shape.part(38).x + shape.part(41).x) / 2)
        leftEyeY = ((shape.part(38).y + shape.part(41).y) / 2)
        rightEyeX = ((shape.part(44).x + shape.part(47).x) / 2)
        rightEyeY = ((shape.part(44).y + shape.part(47).y) / 2)
        noseTipX = shape.part(30).x
        noseTipY = shape.part(30).y
        #		mouthX=shape.part(51).x
        #		mouthY=shape.part(51).y
        mouthX = shape.part(62).x
        mouthY = shape.part(62).y

        #		chinX =shape.part(8).x
        #		chinY =shape.part(8).y
        #		headLX=shape.part(0).x
        #		headLY=shape.part(0).y
        #		headRX=shape.part(16).x
        #		headRY=shape.part(16).y

        eye_distance = rightEyeX - leftEyeX

        if abs(leftEyeY - rightEyeY) / 100 * eye_distance <= 5:  ### eyes seem horizontal
            #	if True:

            xxx = ImageX
            yyy = ImageY
            www = ImageW
            hhh = ImageH

            if noseTipX > 0:  ### nose found

                if mouthY > 0:  ### dummy

                    eye_distance = rightEyeX - leftEyeX
                    eyeDistance = eye_distance
                    # eye_nose_distance =  noseTipY - (rightEyeY+leftEyeY)/2
                    eye_mouth_distance = mouthY - (rightEyeY + leftEyeY) / 2

                    dleft = eye_distance * 0.5
                    dright = eye_distance * 0.5
                    dtop = eye_mouth_distance * 1.5  # 2.0	#1.4
                    dbottom = eye_mouth_distance * 0.6

                    cropX = leftEyeX - dleft
                    cropW = rightEyeX + dright - cropX
                    cropY = mouthY - dtop
                    cropH = mouthY + dbottom - cropY

                    imgCrop = img[
                              int(cropY):int(cropH + cropY),
                              int(cropX):int(cropW + cropX)
                              ]

                    ImageX = (int)(cropX)
                    ImageY = (int)(cropY)
                    ImageW = (int)(cropW)
                    ImageH = (int)(cropH)
                    try:
                        gray1 = cv2.cvtColor(imgCrop, cv2.COLOR_BGR2GRAY)
                    except:
                        gray1=imgCrop
                    # cv2.imwrite("./img/pic6.png", gray1)
                    imgNormalized = cv2.resize(gray1, (128, 128))
                    # cv2.imwrite("./img/pic5.png", imgNormalized)
                    imgNormalized = applyGreen(imgNormalized)
                    # cv2.imwrite("./img/pic7.png", imgNormalized)
                    # imgNormalized2=cv2.resize(imgCrop2, (NORMALIZE_DIMENSION,NORMALIZE_DIMENSION))
                    resized_face.append(imgNormalized)
                    # resized_face.append(gray1)

                    faceOnScreen = True
    # print len(resized_face)
    # print resized_face
    # cv2.imwrite("./small_face/"+str(time.time())+".jpg",resized_face[0])

    return resized_face

def DetectFaces(drawImg, img, scale, scaleY):
    nn0 = 130  #162 # 220	#350
    nn1 = 130  #349 # 120
    nn2 = 1
    maxx0=99999
    maxy0=99999
    maxx1=0
    maxy1=0
    results = []
    rimg=[]
    box = []
    faces = []
    h, w = img.shape[:2]
    wImg = cv2.resize(img, ((int)(w * scale), (int)(h * scale * scaleY)), interpolation=cv2.INTER_CUBIC)
    faces = FaceDetector.detectMultiScale(wImg, 1.01, 1, 0, minSize=(nn0, (int)(nn0 * nn2)),maxSize=(nn1, (int)(nn1 * nn2)))

    for x, y, w, h in faces:
        box = dlib.rectangle(left=(int)(x), top=(int)(y), right=(int)(x + w), bottom=(int)(y + h))
        shape = FacePredictor(wImg, box)
        if shape:
            if False:
                for i in range(1, 68):
                    cv2.circle(drawImg, ((int)((shape.part(i).x) / scale), (int)((shape.part(i).y) / scale / scaleY)),
                               5, (0, 255, 0), 2)

            leftEyeX = (int)(((shape.part(38).x + shape.part(41).x) / 2) / scale)
            leftEyeY = (int)(((shape.part(38).y + shape.part(41).y) / 2) / scale / scaleY)
            rightEyeX = (int)(((shape.part(44).x + shape.part(47).x) / 2) / scale)
            rightEyeY = (int)(((shape.part(44).y + shape.part(47).y) / 2) / scale / scaleY)
            if False:
                cv2.circle(drawImg, ((int)((leftEyeX) / scale), (int)((leftEyeY) / scale / scaleY)), 20, (0, 255, 0), 2)
                cv2.circle(drawImg, ((int)((rightEyeX) / scale), (int)((rightEyeY) / scale / scaleY)), 20, (0, 255, 0),
                           2)
            # mouthX=(int)(shape.part(62).x/scale)
            mouthY = (int)(shape.part(62).y / scale / scaleY)
            EyesCenterX = (leftEyeX + rightEyeX) / 2
            EyesCenterY = (leftEyeY + rightEyeY) / 2
            EyesDistance = (rightEyeX - leftEyeX)
            EyeMouthDistance = mouthY - EyesCenterY
            x0 = (int)(leftEyeX - EyesDistance * 1.5)
            x1 = (int)(rightEyeX + EyesDistance * 1.5)
            y0 = (int)(EyesCenterY - EyeMouthDistance * 1.8)
            y1 = (int)(mouthY + EyeMouthDistance * 1.8)
            m0 = y1 - y0
            m1 = x1 - x0
            aspect = float(m1) / float(m0)
            if (m0 >= m1) & (x0 > 0) & (y0 > 0):
                if x0<maxx0:
                    maxx0=x0
                if y0 < maxy0:
                    maxy0 = y0
                if x1 > maxx1:
                    maxx1 = x1
                if y1 > maxy1:
                    maxy1 = y1
                rimg = img[maxy0:maxy1, maxx0:maxx1]
                results.append(rimg)

    return rimg, list(faces)

def greenMask():
    greenMask = np.zeros((128, 128), dtype=np.float64)
    xxx, yyy = greenMask.shape[:2]
    gammaH = np.float64(6)
    gammaL = np.float64(0.05)
    C = np.float64(1)
    d0 = np.float64((xxx / 2) * (xxx / 2) + (yyy / 2) * (yyy / 2))
    d2 = np.float64(0)
    for y in range(int(yyy)):
        for x in range(int(xxx)):
            d2 = np.float64(x * x + y * y)
            greenMask[x, y] = (gammaH - gammaL) * (1 - np.exp(-C * d2 / d0)) + gammaL
    greenMask[0, 0] = np.float64(1.1)
    return greenMask

def applyGreen(mat):
    h, w = mat.shape[:2]
    aspect = float(w) / float(h)
    tmp = cv2.resize(mat, (128, 128), interpolation=cv2.INTER_CUBIC)
    # cv2.imwrite("./log_img/"+str(time.time)+".jpg",tmp)
    # cv2.waitKey(30)
    # cv2.imshow("test111",tmp)
    #clahe = cv2.createCLAHE(clipLimit=FF_BW_CLIPLIMIT, tileGridSize=(FF_BW_TILEGRIDSIZE_0, FF_BW_TILEGRIDSIZE_1))
    #gray2 = clahe.apply(tmp)
    tmp = cv2.GaussianBlur(tmp, (15, 15), 0)
    tmp = np.float64(tmp)
    tmp = cv2.dct(tmp)
    tmp = np.uint8(cv2.idct(tmp * greenMask()))

    tmp = cv2.equalizeHist(tmp)
    tmp = cv2.GaussianBlur(tmp, (15, 15), 0)
    return tmp

# def InitKnowledge():
#     global start_ready
#     print "init knowledge ..."
#     labels = []
#     images = []
#     for path in os.listdir(cidevice_face):
#         kPath = cidevice_face + path + "/"
#         wwwid = int(path)
#         if len(os.listdir(str(kPath)))>0:
#             registeredWorkerID.append(wwwid)  ###	<----------------------
#             handicappedFaceRecognizer.append(cv2.face.createLBPHFaceRecognizer(radius=1, neighbors=8, grid_x=8, grid_y=12,threshold=MAX_REG_CONF_REJECT))
#             for ifile in os.listdir(kPath):
#                 wwwfn = kPath + ifile
#                 img = cv2.cvtColor(cv2.imread(wwwfn, 1), cv2.COLOR_BGR2GRAY)
#                 img = applyGreen(img)
#                 labels.append(wwwid)
#                 images.append(img)
#     print "training..."
#     FaceRecognizer.train(images, np.array(labels))
#
#     # print "saving..."
#     # FaceRecognizer.save("./cw.net")
#     print len(registeredWorkerID)
#     for n, workerID in enumerate(registeredWorkerID):
#         handiLabels = []
#         handiImages = []
#         print "handicapping " + str(workerID)
#         for i, label in enumerate(labels):
#             if (workerID != label):
#                 handiLabels.append(label)
#                 handiImages.append(images[i])
#         try:
#             handicappedFaceRecognizer[n].train(handiImages, np.array(handiLabels))
#         except Exception as err:
#             print"handicapping error "
#             print err
#             return
#
#         start_ready+=(1.0/len(registeredWorkerID) * 100)
#         print "%.2f%%" % start_ready

def one_InitKnowledge(facePath,faceDir):
    global start_ready
    one_face_mode = cv2.face.createLBPHFaceRecognizer(radius=1, neighbors=8, grid_x=8, grid_y=12,threshold=MAX_REG_CONF_REJECT)
    # print "init knowledge ..."
    labels = []
    images = []
    kPath =str(facePath + "/")
    kFile = os.listdir(kPath)
    if len(kFile) > 10:
        kFile = kFile[1:]
    if len(kFile)>0:
        wwwid=int(faceDir)
        one_registeredWorkerID.append(wwwid)  ###	<----------------------
        for i in kFile:
            wwwfn = kPath + i
            if os.path.getsize(wwwfn)>0:
                img = cv2.cvtColor(cv2.imread(wwwfn, 1), cv2.COLOR_BGR2GRAY)
                img = applyGreen(img)
                labels.append(wwwid)
                images.append(img)
            else:
                pass
    # print "training..."
    one_face_mode.train(images, np.array(labels))
    return one_face_mode

def one_work_face_ciface(img,facePath,faceDir):
    # global FaceRecognizer
    #Jason
    # workid=0
    # like=999
    #Jason End
    try:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    except:
        gray=img
    # cv2.imshow("imggg", gray)
    # cv2.waitKey(600)
    grayX = applyGreen(gray)
    # cv2.imshow("imggg",grayX)
    # cv2.waitKey(600)
    # print  type(FaceRecognizer)
    last_predicted, mac_index = one_InitKnowledge(facePath,faceDir).predict(grayX)
    return last_predicted, mac_index

class FaceDetect:

    def __init__(self,frame, screen, config):
        self.stopped = False
        self.frame = frame
        self.serverDetectFace = []
        self.localDetectFace = []
        self.displayFace = []
        # self.face = []
        self.img = []
        self.faceFrame = []
        self.workerID = ''
        self.localStart = 0
        self.serverStart = 0
        self.serverScore = 0
        self.localScore = 1000
        self.screen = screen
        self.localFaceDir = config.pbox_localFaceDir
        self.companyID = config.serial_companyID
        self.FACESERVER = config.server_FACESERVER
        self.face_compare = config.server_face_compare
        # self.cap = WebcamVideoStream(src=0).start()

    def start(self):
        threading.Thread(target=self.run, args=()).start()

    def stop(self):
        self.stopped = True

    def run(self):
        # logic_set = time.time()
        detectedFace = []
        detectedFace1 = []
        print "[FD] =====Face detection start====="
        while 1:
            if self.stopped:
                return
            frame = self.frame.read()
            # img = cv2.flip(frame, 1)
            self.img = rotate_img(frame, 90)
            img1 = self.img
            img2 = img1.copy()
            detectedFace, self.faceFrame = DetectFaces(img2, img1, 1.0, 1.0)

            # if len(detectedFace) > 0:
            #
            #     detectedFace1 = faceDetectForRecognition(detectedFace)
            #     self.canface = detectedFace
            #     self.canface = cv2.cvtColor(self.canface, cv2.COLOR_BGR2RGBA)
            #     self.screen.setAtr('face', self.canface)
            # else:
            #     detectedFace1 = faceDetectForRecognition(cropped_img)
            # print "[FM]",self.faceFrame
            if self.workerID != '':

                if len(detectedFace) > 0 and len(detectedFace) < 700:
                    facePic = detectedFace
                else:
                    facePic = self.img

                if self.serverStart == 0:
                    self.serverStart = 1
                    self.serverDetectFace = facePic
                    threading.Thread(target=self.serverRecognition, args=(self.serverDetectFace, self.workerID, self.companyID)).start()
                if self.localStart == 0 :
                    self.localStart = 1
                    self.localDetectFace = facePic
                    threading.Thread(target=self.localRecognition, args=(self.localDetectFace, self.workerID)).start()
            else:
                self.serverScore = 0
                self.localScore = 1000

            if len(detectedFace) > 0 and len(detectedFace) < 700:
                    try:
                        face1 = face_gleass.detectMultiScale(detectedFace, 2, 3)
                        canface = detectedFace
                        self.displayFace = cv2.cvtColor(canface, cv2.COLOR_BGR2RGBA)
                        # cv2.imwrite("./img/pic3.png", self.canface)
                        self.screen.set('face', self.displayFace)
                        # print "face found"
                    except:
                        continue
                    if len(face1) < 2:
                        continue
            elif len(detectedFace1) > 700:

                cccc = 0
                xxx = 0
                for i in detectedFace1:
                    if cccc < 1:
                        for x in i:
                            xxx += 1
                    cccc += 1
                canface = detectedFace1[0:xxx, 0:cccc / 2]
                # canface = cv2.resize(canface, (300, 240))
                self.displayFace = cv2.cvtColor(canface, cv2.COLOR_BGR2RGBA)
                self.screen.set('face', self.displayFace)
                print "found top face"
            else:
                pass

            if len(self.displayFace) > 0:
                # self.face = rotate_img(self.face, 90)
                # self.canface = cv2.resize(self.face, (240, 300))
                # print "canface ready"
                # cv2.imshow('CiSystem', self.canface)
                # cv2.moveWindow('CiSystem', 0, 0)
                # cv2.waitKey(1)
                time.sleep(0.1)
                # self.canface = []
            else:
                pass

    def localRecognition(self, img, ID):
        print "[FD] =====Local recognition start====="
        localDetectFace = img
        workerID = ID
        facePath = ''
        localFace = []

        if len(workerID) > 8:
            faceDir = str('3' + workerID[len(workerID) - 7:len(workerID)])
            facePath = str(self.localFaceDir + faceDir)

        localFace = faceDetectForRecognition(localDetectFace)

        if len(localFace) > 0:
            # self.face = localFace[0]
            canface = cv2.cvtColor(localFace[0], cv2.COLOR_GRAY2BGRA)
            self.screen.set('smallFace', canface)
            if os.path.exists(facePath):
                if len(os.listdir(facePath)):
                    one_found, score = one_work_face_ciface(localFace[0], facePath, faceDir)
                    self.localScore = int(score)
                    # self.localReturn = int(one_found)
                    self.screen.set('score', str(self.localScore))
                else:
                    print "[FD] Local no comparison face"
                    self.localScore = 500
                    self.screen.set('score', self.localScore)
            else:
                try:
                    os.mkdir(facePath)
                except:
                    pass
        else:
            print "[FD] Local no face found"
            self.localScore = 800
            self.screen.set('score', self.localScore)

        print "[FD] Local Score: ", self.localScore
        if self.localScore > 150:
            self.localStart = 0

    def serverRecognition(self, img, workerID, companyID):

        print "[FD] =====Server recognition start====="
        serverDetectFace = img
        try:
            data = {"IDPic": str(workerID), "FacePic": opencv_base64(serverDetectFace), "siteid": companyID}
            # json_str = json.dumps(data)
            print "[FD] Server recognition request at: " + str(time.time())
            # main_logger.info("begin request time :" + str(time.time()))
            # main_logger.info("begin request img :" + str(begintime) + ".jpg")
            # r = requests.post('http://ec2-52-221-20-72.ap-southeast-1.compute.amazonaws.com:8118/FaceService/Search_Face',json_str)
            start = time.time()
            r = requests.post(self.FACESERVER + self.face_compare, json=data, timeout=500)
            # print r
            # print r.text
            # print (time.time() - start)
            # main_logger.info("server found use time :" + str((time.time()-start)))
            rx = r.json()
            # main_logger.info("return json :" + str(rx))

            print "[FD] Server return at: " + str(time.time())
            print "[FD] Server return value: " + str(rx)
            # main_logger.info("end request time :" + str(time.time()))
            print rx
            x = rx["state"]

            if x == 0:
                like1 = rx['result']
                # main_logger.info("server like :" + str(like1))
                # print like1
                like1 = str(like1)
                try:
                    like1 = like1[:2]
                    int(like1)
                except:
                    like1 = like1[:1]
                # server_return = str(workerID)
                # server_like = str(like1)
                self.serverScore = like1
                print "[FD] Server return score: ", self.serverScore

                # print type(like1)
                # if old_inputid==workerID:
                #     old_card+=5
                # else:
                #     old_inputid=workerID
                #     old_card=0
                # if old_card>15:
                #     old_card=0
                # if int(like1) > int(server_emperor):
                #     # if int(like1)>(int(server_emperor)-old_card):
                #     # uesr_name1 = rx['uesr_name'].encode("UTF-8")
                #     # uesr_name1 = str(uesr_name1)[18: -4]
                #     # print uesr_name1
                #     # print like1
                #     # mylock.acquire()
                #     # uesr_name = "not"
                #     # FACE_WORKID = workerID
                #     # mylock.release()
                #
                #     # if not os.path.exists(cidevice_face + str(faceID)):
                #     #     os.makedirs(cidevice_face + str(faceID))
                #     print "face id " + str(workerID)
                #     set_time = 0
                #
                #     if len(imgxxx) > 0 and int(like1) > int(server_save):
                #         try:
                #             os.mkdir("./cidevice_face/")
                #         except:
                #             pass
                #         try:
                #             os.mkdir(str(cidevice_face + str(faceID)))
                #         except:
                #             pass
                #         if len(os.listdir(str(cidevice_face + str(faceID)))) > logcal_more:
                #             for nbr in os.listdir(cidevice_face + str(faceID)):
                #                 files = os.stat(cidevice_face + str(faceID) + "/" + str(nbr))
                #                 if set_time == 0:
                #                     set_time = files.st_mtime
                #                     last = nbr
                #                 if set_time > files.st_mtime:
                #                     set_time = files.st_mtime
                #                     last = nbr
                #                     print last
                #             mqsave(
                #                 "?employeeID=" + str(faceID) + "&imageNames=" + str(Serial_Number) + "_pic_" + str(
                #                     last)[:-4] + "&serial_number=" + str(Serial_Number) + "&siteID=" + str(Site_id),
                #                 mqdel_image_by_imageName)
                #             os.remove(cidevice_face + str(str(faceID) + "/" + str(last)))
                #         # main_logger.info("save logic  time :" + str(begintime))
                #         cv2.imwrite(cidevice_face + str(faceID) + "/" + str(begintime) + ".jpg", imgxxx)
                #         install_mq = {
                #             "employeeID": str(faceID),
                #             "imageNames": [str(Serial_Number) + "_pic_" + str(begintime)],
                #             "serial_number": str(Serial_Number),
                #             "siteID": Site_id
                #         }
                #         install_mq = json.dumps(install_mq)
                #         try:
                #             print "mqsavedevice success "
                #             mqsave(install_mq, mqsavedevice)
                #         except:
                #             print "mqsavedevice error"
                #             pass
                #
                #         install_mqqq = {
                #             "imageNames": (str(Serial_Number) + "_pic_" + str(begintime)),
                #             "data": opencv_base64(imgxxx)
                #         }
                #         install_mqqqq = json.dumps(install_mqqq)
                #         try:
                #             mqsave(install_mqqqq, mqsave_image)
                #         except:
                #             print "mqsave error"
                #             pass
                #     return ""
                # else:
                #     time.sleep(1)
                #     mylock.acquire()
                #     # main_logger.info("server like :" + str(0))
                #     FACE_WORKID = "00000000"
                #     mylock.release()
            else:
                print "[FD] Server bad result"
                self.serverScore = 1
                print "[FD] Server score: ", self.serverScore
                # time.sleep(0.1)
                # # main_logger.info("server like :" + str(0))
            self.localStart = 1


        except Exception as err:
            print "[FD] Server recognition err"
            time.sleep(0.1)
            # main_logger.info("server error  :" + str(err))
            print(err)
        return self

    def setID(self, value):
        self.workerID = value

    def clear(self):
        self.workerID = ''
        self.serverStart = 0
        self.localStart = 0
        self.serverScore = 0
        self.localScore = 1000
        self.localDetectFace = []
        self.serverDetectFace = []
        # self.face = []

    def getSmallFace(self):
        saveCanFace = []
        localFace = faceDetectForRecognition(self.serverDetectFace)
        if len(localFace) > 0:
            saveCanFace = cv2.cvtColor(localFace[0], cv2.COLOR_GRAY2BGRA)
        return saveCanFace

    def getFace(self):
        return self.localDetectFace, self.serverDetectFace

    def getScore(self):
        return  int(self.localScore), int(self.serverScore)

if __name__ == "__main__":
    detection = FaceDetect().start()
    starttime = time.time()
    while True:
        if time.time() - starttime > 10:
            print ("End time: ", time.time)
            break

