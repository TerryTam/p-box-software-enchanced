
import cv2
import os
import time
import threading

class lightAdjust(threading.Thread):

    def __init__(self, config, videocap, detection):
        threading.Thread.__init__(self)
        self.config = config
        self.cap = videocap
        self.detection = detection

    def run(self):
        print "=====[LA] Light adjustment running====="
        global xl1
        global xl2
        global yl1
        global yl2
        global lux_low
        global lux_high
        CAP_RESOLUTION_WIDTH = 640
        CAP_RESOLUTION_HEIGHT = 480
        a_xl1 = self.config.pbox_light_xl1  # 120 #80
        a_xl2 = self.config.pbox_light_xl2  # 400
        a_yl1 = self.config.pbox_light_yl1  # 180 #140
        a_yl2 = self.config.pbox_light_yl2  # 460 #500

        lux_low = self.config.pbox_light_lux_low
        lux_high = self.config.pbox_light_lux_high

        os.popen("uvcdynctrl -s 'Exposure, Auto' 1 ")
        while (1):
            time.sleep(0.1)
            box = []
            img = self.cap.read()
            box = self.detection.faceFrame
            if box != []:
                box = box[0]
                x0 = box[0]
                y0 = box[1]
                x1 = x0 + box[2]
                y1 = y0 + box[3]
                img = img[x0:x1,y0:y1]
                xl1 = 0
                xl2 = box[2]
                yl1 = 0
                yl2 = box[3]
            else:
                xl1 = a_xl1
                xl2 = a_xl2
                yl1 = a_yl1
                yl2 = a_yl2
            lux_back = 0
            total_back_pix = 0
            lux_mid = 0
            total_mid_pix = 0
            # img = cv2.flip(img, 1)
            # img_flipped = cv2.resize(img, (CAM_RESOLUTION_WIDTH, CAM_RESOLUTION_HEIGHT))

            # yl1= int(CAM_RESOLUTION_WIDTH*0.15)
            # yl2= int(CAM_RESOLUTION_WIDTH*0.85)
            # xl1= int(CAM_RESOLUTION_HEIGHT*0.25)
            # xl2= int(CAM_RESOLUTION_HEIGHT*0.75)

            # hsv = cv2.cvtColor(img_flipped, cv2.COLOR_BGR2HSV)
            hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

            for x in range(xl1, xl2):
                for y in range(yl1, yl2):
                    lux_mid += hsv[x, y, 2]
                    total_mid_pix += 1
            avg_lux = lux_mid / total_mid_pix

            # for x in range (0, CAP_RESOLUTION_HEIGHT):
            # for y in range (0, CAP_RESOLUTION_WIDTH):
            # if xl1<x<xl2 and yl1<y<yl2:
            # lux_mid+=hsv[x,y,2]
            # total_mid_pix+=1
            # else:
            # lux_back+=hsv[x,y,2]
            # total_back_pix+=1

            # for x in range (0, CAM_RESOLUTION_HEIGHT):
            # for y in range (0, CAM_RESOLUTION_WIDTH):
            # if xl1<x<xl2 and yl1<y<yl2:
            # lux_mid+=hsv[x,y,2]
            # total_mid_pix+=1
            # else:
            # lux_back+=hsv[x,y,2]
            # total_back_pix+=1


            # avg_mid_lux=lux_mid/total_mid_pix
            # avg_back_lux=lux_back/total_back_pix
            ##avg_lux=(avg_mid_lux+avg_back_lux*0.5)/1.5
            # avg_lux=avg_mid_lux*0.7+avg_back_lux*0.3

            # print "avg_mid_lux="+str(avg_mid_lux)
            # print "avg_back_lux="+str(avg_back_lux)
            # print "[LA] avg_lux=" + str(avg_lux)

            if avg_lux >= lux_low and avg_lux < lux_high:
                pass
            else:

                nowd = os.popen("uvcdynctrl -g 'Exposure (Absolute)'")
                nowd = nowd.read()
                nowd = float(nowd)
                # print "nowd = " + str(nowd)
                nowb = os.popen("uvcdynctrl -g 'Brightness'")
                nowb = nowb.read()
                nowb = int(nowb)
                # print "nowb = " + str(nowb)

                if avg_lux < lux_low - 115:  # 50:
                    if nowd < 10:
                        nowd = nowd + 1
                    elif nowd < 200:
                        nowd = nowd * 2
                    elif nowd < 500:
                        nowd = nowd * 1.4
                    elif nowd < 1000:
                        nowd = nowd * 1.3
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 7
                    elif nowd < 3000:
                        nowd = nowd * 1.2
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_low - 115 and avg_lux < lux_low - 85:  # avg_lux >= 50 and avg_lux < 80:
                    if nowd < 10:
                        nowd = nowd + 1
                    elif nowd < 200:
                        nowd = nowd * 2
                    elif nowd < 500:
                        nowd = nowd * 1.36
                    elif nowd < 1000:
                        nowd = nowd * 1.27
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 5
                    elif nowd < 3000:
                        nowd = nowd * 1.15
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_low - 85 and avg_lux < lux_low - 65:  # avg_lux >= 80 and avg_lux < 100:
                    if nowd < 10:
                        nowd = nowd + 1
                    elif nowd < 200:
                        nowd = nowd * 2
                    elif nowd < 500:
                        nowd = nowd * 1.33
                    elif nowd < 1000:
                        nowd = nowd * 1.24
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 5
                    elif nowd < 3000:
                        nowd = nowd * 1.12
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_low - 65 and avg_lux < lux_low - 45:  # avg_lux >= 100 and avg_lux < 120:
                    if nowd < 10:
                        nowd = nowd + 1
                    elif nowd < 200:
                        nowd = nowd * 2
                    elif nowd < 500:
                        nowd = nowd * 1.3
                    elif nowd < 1000:
                        nowd = nowd * 1.2
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 5
                    elif nowd < 3000:
                        nowd = nowd * 1.08
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_low - 45 and avg_lux < lux_low - 35:  # avg_lux >= 120 and avg_lux < 130:
                    if nowd < 10:
                        nowd = nowd + 1
                    elif nowd < 20:
                        nowd = nowd * 1.8
                    elif nowd < 500:
                        nowd = nowd * 1.26
                    elif nowd < 1000:
                        nowd = nowd * 1.18
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 5
                    elif nowd < 3000:
                        nowd = nowd * 1.05
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_low - 35 and avg_lux < lux_low - 25:  # avg_lux >= 130 and avg_lux < 140:
                    if nowd < 10:
                        nowd = nowd + 1
                    elif nowd < 200:
                        nowd = nowd * 1.7
                    elif nowd < 500:
                        nowd = nowd * 1.23
                    elif nowd < 1000:
                        nowd = nowd * 1.15
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 4
                    elif nowd < 3000:
                        nowd = nowd * 1.05
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_low - 25 and avg_lux < lux_low - 15:  # avg_lux >= 140 and avg_lux < 150:
                    if nowd < 10:
                        nowd = nowd + 1
                    elif nowd < 200:
                        nowd = nowd + 1.5
                    elif nowd < 500:
                        nowd = nowd * 1.2
                    elif nowd < 1000:
                        nowd = nowd * 1.12
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 4
                    elif nowd < 3000:
                        nowd = nowd * 1.04
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_low - 15 and avg_lux < lux_low - 5:  # avg_lux >= 150 and avg_lux < 160:
                    if nowd < 10:
                        nowd = nowd + 1
                    elif nowd < 200:
                        nowd = nowd * 1.2
                    elif nowd < 500:
                        nowd = nowd * 1.15
                    elif nowd < 1000:
                        nowd = nowd * 1.1
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 4
                    elif nowd < 3000:
                        nowd = nowd * 1.03
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_low - 5 and avg_lux < lux_low:  # avg_lux >= 160 and avg_lux < 165:
                    if nowd < 30:
                        nowd = nowd + 1
                    elif nowd < 500:
                        nowd = nowd * 1.07
                    elif nowd < 1000:
                        nowd = nowd * 1.05
                        if nowb < 40:
                            nowb = 40
                        elif nowb < 60:
                            nowb = nowb + 2
                    elif nowd < 3000:
                        nowd = nowd * 1.02
                        nowb = 60
                    else:
                        nowd = 3000
                        nowb = 60

                elif avg_lux >= lux_high + 1 and avg_lux < lux_high + 10:  # avg_lux >= 168 and avg_lux < 178:
                    if nowd < 2:
                        nowd = 1
                        if nowb > 5 and nowd > 500:
                            nowb = nowb - 5
                    else:
                        nowd = nowd * 0.95

                elif avg_lux >= lux_high + 10 and avg_lux < lux_high + 20:  # avg_lux >= 178 and avg_lux < 190:
                    if nowd < 2:
                        nowd = 1
                        if nowb > 10 and nowd > 500:
                            nowb = nowb - 5
                    else:
                        nowd = nowd * 0.9

                elif avg_lux >= lux_high + 20 and avg_lux < lux_high + 30:  # avg_lux >= 190 and avg_lux < 200:
                    nowd = nowd * 0.85
                    if nowb > 20 and nowd > 500:
                        nowb = 20

                elif avg_lux >= lux_high + 30 and avg_lux < lux_high + 40:  # avg_lux >= 200 and avg_lux < 210:
                    nowd = nowd * 0.8
                    if nowb > 20 and nowd > 500:
                        nowb = 20

                elif avg_lux >= lux_high + 40 and avg_lux < lux_high + 50:  # avg_lux >= 210 and avg_lux < 220:
                    nowd = nowd * 0.75
                    if nowb > 20 and nowd > 500:
                        nowb = 20

                elif avg_lux >= lux_high + 50 and avg_lux < lux_high + 60:  # avg_lux >= 220 and avg_lux < 230:
                    nowd = nowd * 0.7
                    if nowb > 20 and nowd > 500:
                        nowb = 20

                elif avg_lux >= lux_high + 60 and avg_lux < lux_high + 70:  # avg_lux >= 230 and avg_lux < 240:
                    nowd = nowd * 0.65
                    if nowb > 20 and nowd > 500:
                        nowb = 20

                elif avg_lux >= lux_high + 70:  # avg_lux >= 240 and avg_lux < 250:
                    nowd = nowd * 0.6
                    if nowb > 20 and nowd > 500:
                        nowb = 20

                os.popen("uvcdynctrl -s 'Brightness' " + str(nowb))
                os.popen("uvcdynctrl -s 'Exposure (Absolute)' " + str(nowd))
