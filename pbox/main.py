# coding:utf-8
import gc
import os
import cv2
import sys
import json
import socket
import time
import logging
import subprocess
import requests
import threading
import ConfigParser
import controller
import numpy as np

from cStringIO import StringIO
from PIL import Image
from imutils.video import WebcamVideoStream
from MassageQueueManager import mqController
from NFCManager import nfcListener
from faceDetectionManager import FaceDetect
from DisplayManager import display, startUpScreen
from lightAdjust import lightAdjust

#=========================================================
reload(sys)
sys.setdefaultencoding('utf8')
#=========================================================
#=========================================================
class pboxConfig:

    def __init__(self):

        print "[Config] Initializing pbox configuration..."

        # =====Loading pbox.ini configuration=====

        self.pbox = ConfigParser.SafeConfigParser()
        self.pbox.read('./pbox_config/pbox.ini')

        self.pbox_localPass = int(self.pbox.get('local', 'local_like_score'))
        self.pbox_localSave = int(self.pbox.get('local', 'local_save_score'))
        self.pbox_serverPass = int(self.pbox.get('local', 'server_like_score'))
        self.pbox_serverSuspect = int(self.pbox.get('local', 'sever_suspect'))
        self.pbox_localImgCount = int(self.pbox.get('local', 'local_img_count'))
        self.pbox_ServerTimeOut = int(self.pbox.get('local', 'server_timeout'))
        self.pbox_localFaceDir = str(self.pbox.get('local', 'cidevice_face'))
        self.pbox_smallOrBig = int(self.pbox.get('local', 'img_capture_ahead'))

        try:
            self.pbox_cicConnected = int(self.pbox.get('local', 'cic_connected'))
        except:
            self.pbox_cicConnected = 0

        self.pbox_light_xl1 = int(self.pbox.get('light', 'xl1'))
        self.pbox_light_xl2 = int(self.pbox.get('light', 'xl2'))
        self.pbox_light_yl1 = int(self.pbox.get('light', 'yl1'))
        self.pbox_light_yl2 = int(self.pbox.get('light', 'yl2'))
        self.pbox_light_lux_low = int(self.pbox.get('light', 'lux_low'))
        self.pbox_light_lux_high = int(self.pbox.get('light', 'lux_high'))

        # =====Loading server.ini configuration=====

        self.server = ConfigParser.SafeConfigParser()
        self.server.read('./pbox_config/server.ini')

        # try:
        #     self.server_logtime = int(self.server.get('facial', 'logtime'))
        # except:
        #     self.server_logtime = int(round(time.time() * 1000))
        #     self.server.set('facial', 'logtime', value=str(self.server_logtime))
        #     self.server.write(open("./pbox_config/server.ini", "w"))

        self.server_logtime = int(self.server.get('facial', 'logtime'))
        self.server_mq_db = str(self.server.get('facial', 'server'))
        self.server_mq_ip = str(self.server.get('mq', 'server'))
        self.server_mq_port = int(str(self.server.get('mq', 'port')))
        self.server_mq_username = str(self.server.get('mq', 'username'))
        self.server_mq_password = str(self.server.get('mq', 'password'))
        self.server_mq_vhost = str(self.server.get('mq', 'vhost'))
        self.server_mq_saveDevice = str(self.server.get('mq', 'save_device'))
        self.server_mq_saveImg = str(self.server.get('mq', 'save_image'))
        self.server_mq_del_image_by_imageName = str(self.server.get('mq', 'mqdel_image_by_imageName'))
        self.server_mq_savelog = str(self.server.get('mq', 'mq_save_doc'))
        self.server_mq_save_log = str(self.server.get('mq', 'mq_save_log'))
        self.server_port = self.server.get('facial', 'PORT')
        self.server_MGDB = self.server.get('backend', 'server')
        self.server_MGDBSEND = self.server.get('backend', 'create')

        self.server_listen_seatus = self.server.get('mq', 'listen_Performance')
        self.server_FACESERVER = self.server.get('facial', 'server')
        self.server_face_register = self.server.get('facial', 'face_register')
        self.server_face_compare = self.server.get('facial', 'face_compare')
        self.server_state = self.server.get('facial', 'health_check')

        # =====Loading serial.ini configuration=====

        self.serial = ConfigParser.SafeConfigParser()
        self.serial.read('./pbox_config/serial_number.ini')
        self.serial_number = self.serial.get('inset', 'Serial_Number')
        self.updateSite()
        self.serial_siteID = self.serial.get('inset', 'SITEID')
        self.serial_numberID = self.serial.get('inset', 'Serial_Number_ID')
        self.serial_companyID = self.serial.get('inset', 'Company_id')
        self.serial_position = self.serial.get('inset', 'Position')
        try:
            self.checkGreenExp = self.serial.get('inset', 'chkGreenExp')
        except:
            self.checkGreenExp = "N"
            # main_logger.info("use_logic_Pbox")
            # main_logger.info(str(err))
        self.sendLog()

    def sendLog(self):
        # nowssned=int(round(logtime * 1000))
        try:
            # print int(round(time.time() * 1000)) - self.server_logtime)
            if int(round(time.time() * 1000)) - self.server_logtime > 86400000:

                data_log = {
                    "logName": str(self.serial_number) + "_log_" + str(self.server_logtime),
                    "serial_number": self.serial_number,
                    "logCreateTime": self.server_logtime,
                    "siteID": self.serial_siteID
                }
                data_logg = json.dumps(data_log)
                mqController.mqsave(data_logg, self.server_mq_savelog)

                send_mq = open("./main_log.txt", "rb").read().encode('base64')
                send_logg = {
                    "logName": str(self.serial_number) + "_log_" + str(self.server_logtime),
                    "data": send_mq
                }
                send_loggg = json.dumps(send_logg)

                try:
                    mqController.mqsave(send_loggg, self.server_mq_save_log)
                    os.remove("./main_log.txt")
                    try:
                        setupLogger('main_logger', './main_log.txt')
                        mainLogger = logging.getLogger("main_logger")
                    except Exception as err:
                        print "main_logger err"
                        print err
                    print "logg success"
                except Exception as err:
                    print err
                    print "logg baddddddddddddddddddddd"
                    pass
                # send_log(logtime)
                self.server.set('facial', 'logtime', value=str(int(round(time.time() * 1000))))
                self.server.write(open("./pbox_config/server.ini", "w"))
                # main_logger.info("send" + str(time.time()))
            else:
                print int(round(time.time() * 1000))
                # main_logger.info("logging is error")
                print "no send error"
        except Exception as err:
            print err
            # main_logger.info("logging is error")
            pass

    def updateSite(self):
        try:
            # print self.MGDB + "/GetdeviceBy_deviceID?Serial_Number=" + str(self.Serial_Number)
            print "[Config] getting site information..."
            r = requests.get(self.server_MGDB + "/GetdeviceBy_deviceID?Serial_Number=" + str(self.serial_number))
            # print Site_r.text
            site_r = r.json()
            siteID = site_r['Site_id']
            serialNumberID = site_r['_id']
            companyID = site_r['Company_id']
            position = site_r['Position']
            checkGreenExp = site_r['chkGreenExp']

            #if site id change, delete the old worker list
            if self.serial.has_option('inset', 'SITEID'):
                self.serial_siteID = self.serial.get('inset', 'SITEID')
                if self.serial_siteID == siteID:
                    pass
                else:
                    if os.path.isfile('worker_list.ini'):
                        os.remove('worker_list.ini')
            else:
                if os.path.isfile('worker_list.ini'):
                    os.remove('worker_list.ini')

            print "[Config] serial_number: ", self.serial_number
            print "[Config] side ID: ", siteID
            print "[Config] saving site information..."
            self.serial.set('inset', 'chkGreenExp', value=str(checkGreenExp))
            self.serial.set('inset', 'SITEID', value=str(siteID))
            self.serial.set('inset', 'Serial_Number_ID', value=str(serialNumberID))
            self.serial.set('inset', 'Company_id', value=str(companyID))
            self.serial.set('inset', 'Position', value=str(position))  # str(Position.encode('UTF-8')))
            # DEVICEID_ID = lp.get('facial', 'DEVICEID_ID')
            # print "serial_number"
            self.serial.write(open("./pbox_config/serial_number.ini", "w"))
            print "[Config] configuration save success"
            # print self.serialConfig.get('inset', 'SITEID')
            # main_logger.info("use_server_Pbox")
            return True
        except Exception as err:
            print "[Config] update site information fault"
            print err
            # main_logger.info("use logic:" + str(err))
            return False

class workerList:

    def __init__(self,config):
        self.config = config
        self.CWRCard = []
        self.whiteCard =[]
        self.tempCard = []
        self.serialMatching = {}
        self.greenCardExpiry = {}

        if self.config.checkGreenExp == 'Y':
            self.checkGreenExp = 1
            print "[main] Green card expiry apply"
        else:
            self.checkGreenExp = 0

        try:
            os.mknod('worker_list.ini')
        except:
            pass

        self.workerSave = ConfigParser.SafeConfigParser()
        self.workerSave.read('worker_list.ini')

        self.load()
        self.update()

    def load(self):

        print "[list] Loading old worker list now.."
        id_temp = []
        if self.workerSave.has_section('allWorker'):
            try:
                id_temp = self.workerSave.get('allWorker', 'cwr_card')
                for i in id_temp.split(','):
                    self.CWRCard.append(str(i).upper())
            except:
                print "[list] No cwr_card option find in allWorker"
            try:
                id_temp = self.workerSave.get('allWorker', 'white_card')
                for i in id_temp.split(','):
                    self.whiteCard.append(str(i).upper())
            except:
                print "[list] No white_card option find in allWorker"
            try:
                id_temp = self.workerSave.get('allWorker', 'temp_card')
                for i in id_temp.split(','):
                    self.tempCard.append(str(i).upper())
            except:
                print "[list] No temp_card option find in allWorker"
        else:
            self.workerSave.add_section('allWorker')

        if self.workerSave.has_section('serialMatching'):
            try:
                self.serialMatching = dict(self.workerSave.items('serialMatching'))
            except:
                print"[list] No option in serial matching"
        else:
            self.workerSave.add_section('serialMatching')

        if self.workerSave.has_section('greenCardExpiry'):
            try:
                self.greenCardExpiry = dict(self.workerSave.items('greenCardExpiry'))
            except:
                print"[list] No option in greenCardExpiry"
        else:
            self.workerSave.add_section('greenCardExpiry')

    def update(self):
        self.updateWorkerList()
        self.updateTempCardList()
        # name = str(listName)
        # if name == "worker":
        #     self.updateWorkerList()
        # elif name == "tempCard":
        #     self.updateTempCardList()
        # else:
        #     print "Input wrong worker list"

    def updateWorkerList(self):

        print "[List] Update worker list"

        try:
            r = requests.get(self.config.server_MGDB + "/getAll_register_numbe?Site_id=" + str(self.config.serial_siteID))

            update = False
            CWRCard_temp = []
            whiteCard_temp = []
            serialMatching_temp = {}
            greenCardExpiry_temp = {}

            for i in r.json():
                if i is None:
                    continue
                #Record CWR number and serial number
                if i["CwrNo"] != None and i["CwrNo"] != "":
                    CWRCard_temp.append(str(i["CwrNo"]))
                    if i["Serial_No"] != "" and i["Serial_No"] != None:
                        CWRCard_temp.append(str(i["Serial_No"]))
                        serialMatching_temp.update({str(i["Serial_No"]):str(i["CwrNo"])})
                        # workerSave_temp.set("serialMatching", str(i["Serial_No"]), value=str(i["CwrNo"]))
                    else:
                        pass
                else:
                    whiteCard_temp.append(str(i["Serial_No"]))

                #Record Green card expiry date if check green card activate
                if self.checkGreenExp==1:
                    if i["GreenCardExpiry"] is not None and i["GreenCardExpiry"] !="":
                        try:
                            gtime = i["GreenCardExpiry"]
                            timeArray = time.strptime(gtime, "%Y-%m-%d")
                            timestamp = time.mktime(timeArray)
                            if timestamp > 1000000000.0:
                                gretime = str(timestamp)
                                if i["CwrNo"] != None and i["CwrNo"] != "":
                                    # workerSave_temp.set("greenCardExpiry", str(i["CwrNo"]), value=gretime)
                                    greenCardExpiry_temp.update({str(i["CwrNo"]):gretime})
                                else:
                                    # workerSave_temp.set("greenCardExpiry", str(i["Serial_No"]), value=gretime)
                                    greenCardExpiry_temp.update({str(i["Serial_No"]):gretime})
                        except Exception as err:
                            print "[list] Green card update err"
                            print "[list] ", err
                            # main_logger.info(str(err))
                            pass
                    else:
                        pass

            #Check and replace the old worker list
            # if len(CWRCard_temp) > len(self.CWRCard):
            #     self.CWRCard = CWRCard_temp
            #     self.workerSave.remove_option('allWorker', 'cwr_card')
            #     self.workerSave.set('allWorker', 'cwr_card', value=str(','.join(CWRCard_temp)))
            #     self.workerSave.write(open('worker_list.ini', 'w'))
            #     update = True
            # if len(whiteCard_temp) > len(self.whiteCard):
            #     self.whiteCard = whiteCard_temp
            #     self.workerSave.remove_option('allWorker', 'white_card')
            #     self.workerSave.set('allWorker', 'white_card', value=str(','.join(whiteCard_temp)))
            #     self.workerSave.write(open('worker_list.ini', 'w'))
            #     update = True
            # if len(serialMatching_temp) > len(self.serialMatching):
            #     self.serialMatching = serialMatching_temp
            #     self.workerSave.remove_option('serialMatching', '')
            #     for i in serialMatching_temp:
            #         self.workerSave.set('serialMatching', str(i), value=serialMatching_temp[i])
            #     self.workerSave.write(open('worker_list.ini', 'w'))
            # if len(greenCardExpiry_temp) > len(self.greenCardExpiry):
            #     self.greenCardExpiry = greenCardExpiry_temp
            #     self.workerSave.remove_option('greenCardExpiry', '')
            #     for i in greenCardExpiry_temp:
            #         self.workerSave.set('greenCardExpiry', str(i), value=greenCardExpiry_temp[i])
            #     self.workerSave.write(open('worker_list.ini', 'w'))


            self.CWRCard = CWRCard_temp
            self.workerSave.remove_option('allWorker', 'cwr_card')
            self.workerSave.set('allWorker', 'cwr_card', value=str(','.join(CWRCard_temp)))

            self.whiteCard = whiteCard_temp
            self.workerSave.remove_option('allWorker', 'white_card')
            self.workerSave.set('allWorker', 'white_card', value=str(','.join(whiteCard_temp)))

            self.serialMatching = serialMatching_temp
            self.workerSave.remove_option('serialMatching', '')
            for i in serialMatching_temp:
                self.workerSave.set('serialMatching', str(i), value=serialMatching_temp[i])

            self.greenCardExpiry = greenCardExpiry_temp
            self.workerSave.remove_option('greenCardExpiry', '')
            for i in greenCardExpiry_temp:
                self.workerSave.set('greenCardExpiry', str(i), value=greenCardExpiry_temp[i])

            self.workerSave.write(open('worker_list.ini', 'w'))

        except Exception as err:
            print "[list] ", err
            print "[list] Update worker list fault"
            # try:
            #     self.worker = []
            #     self.tempCard = []
            #     self.greenCardDeadline = {}
            #
            #     self.worker = self.workerSave.get("all_worker", "longWorker")
            #     self.tempCard = self.workerSave.get("all_worker", "tempWorker")
            #     if self.start_greey == 1:
            #         self.greenCardDeadline = self.workerSave.items("greey_card")
            # except:
            #     print "[list] Loading old worker list fault"


        # try:
        #
        #     r = requests.get(self.config.MGDB + "/getAll_register_numbe?Site_id=" + str(self.config.Site_id))
        #     # print self.config.MGDB + "/getAll_register_numbe?Site_id=" + str(self.config.Site_id)
        #
        #     self.worker = []
        #     all_greey = []
        #     all_name = {}
        #     all_greeytime = {}
        #
        #     try:
        #         self.workerSave.remove_section("all_worker")
        #         self.workerSave.add_section("all_worker")
        #         self.workerSave.remove_section("greey_card")
        #         self.workerSave.add_section("greey_card")
        #     except:
        #         pass
        #
        #     for i in r.json():
        #         if i["CwrNo"] != None and i["CwrNo"] != "":
        #
        #             # print "CwrNo!=None"
        #             if i["NameChi"] == "" or i["NameChi"] == None and i["GreenCardExpiry"] != "":
        #                 # print i["NameEng"]
        #                 self.worker.append(str(i["CwrNo"]))
        #                 self.workerSave.set("all_worker", str(i["CwrNo"]), value=i["NameEng"])
        #                 all_name[str(i["CwrNo"])] = i["NameEng"]
        #             else:
        #                 self.worker.append(str(i["CwrNo"]))
        #                 self.workerSave.set("all_worker", str(i["CwrNo"]), value=i["NameChi"])
        #                 all_name[str(i["CwrNo"])] = i["NameChi"]
        #
        #             if i["GreenCardExpiry"] != None and self.start_greey == 1:
        #                 try:
        #                     gtime = i["GreenCardExpiry"]
        #                     timeArray = time.strptime(gtime, "%Y-%m-%d")
        #                     timestamp = time.mktime(timeArray)
        #                     if timestamp > 1000000000.0:
        #                         gretime = str(timestamp)
        #                         all_greey.append(i["CwrNo"])
        #                         self.workerSave.set("greey_card", str(i["CwrNo"]), value=gretime)
        #                         all_greeytime[str(i["CwrNo"])] = gretime
        #                 except Exception as err:
        #                     # main_logger.info(str(err))
        #                     pass
        #
        #         else:
        #             # print "CwrNo=None"
        #             if i["NameChi"] == "" or i["NameChi"] == None:
        #                 # print i["NameEng"]
        #                 self.worker.append(str(i["Serial_No"]))
        #                 self.workerSave.set("all_worker", str(i["Serial_No"]), value=i["NameEng"])
        #                 all_name[str(i["Serial_No"])] = i["NameEng"]
        #             else:
        #                 self.worker.append(str(i["Serial_No"]))
        #                 self.workerSave.set("all_worker", str(i["Serial_No"]), value=i["NameChi"])
        #                 all_name[str(i["Serial_No"])] = i["NameChi"]
        #             if i["GreenCardExpiry"] != None and self.start_greey == 1 and i["GreenCardExpiry"] != "":
        #                 try:
        #                     gtime = i["GreenCardExpiry"]
        #                     timeArray = time.strptime(gtime, "%Y-%m-%d")
        #                     timestamp = time.mktime(timeArray)
        #                     if timestamp > 1000000000.0:
        #                         gretime = str(timestamp)
        #                         all_greey.append(i["Serial_No"])
        #                         self.workerSave.set("greey_card", str(i["Serial_No"]), value=gretime)
        #                         all_greeytime[str(i["Serial_No"])] = gretime
        #                 except Exception as err:
        #                     # main_logger.info(str(err))
        #                     pass
        #
        #     # print self.worker
        #     # print all_name
        #     self.workerSave.write(open("logic_id.ini", "w"))
        #     # main_logger.info("server all_user")
        #     print "[List] worker list ready"
        # except Exception as err:
        #     self.worker = []
        #     all_greey = []
        #     loc_all_gg = []
        #     print err
        #     print "[List]   worker list update fault"
        #     # main_logger.info("logic all_user")
        #     loc_all_uu = self.workerSave.options("all_worker")
        #
        #     for all_u in loc_all_uu:
        #         au = all_u.upper()
        #         self.worker.append(au)
        #         all_name[au] = self.workerSave.get("all_worker", all_u)
        #     try:
        #         loc_all_gg = self.workerSave.options("greey_card")
        #         for all_g in loc_all_gg:
        #             ag = all_g.upper()
        #             all_greey.append(ag)
        #             all_greeytime[all_g] = self.workerSave.get("greey_card", all_g)
        #     except:
        #         print "err aobot all_g"
        #         pass

    def updateTempCardList(self):
        # print self.config.MGDB + "/get_Universal_card?Site_id=" + str(self.config.Site_id)
        print "[List] update TempCard list"

        try:
            r = requests.get(self.config.server_MGDB + "/get_Universal_card?Site_id=" + str(self.config.serial_siteID))
            TempCardList = r.json()
            tempCard_temp = []

            if TempCardList['Universal_card'] != []:
                for x in TempCardList['Universal_card']:
                    tempCard_temp.append(str(x))
                    # main_logger.info("server wan_worker")
                # if len(tempCard_temp) > len(self.tempCard):
                #     self.tempCard = tempCard_temp
                #     for i in tempCard_temp:
                #         self.workerSave.set('allWorker', tempCard_temp[i], value='0')
                #     self.workerSave.write(open('worker_list.ini', 'w'))
                self.tempCard = tempCard_temp
                self.workerSave.write(open('worker_list.ini', 'w'))
                print "[List] Temp card list ready"
            else:
                self.tempCard = tempCard_temp
                self.workerSave.write(open('worker_list.ini', 'w'))
                print "[List] No temp card find"

        except Exception as err:
            # self.tempCard = []
            print err
            print "[list] Update temp card list fault"
            # main_logger.info("logic wan_worker")
            # print "[List] temp card list update fault"
            # self.tempCard = self.workerSave.options("site_model")
            pass

class in_sql:
    # def __init__(self, config, mq, img, workinput, user_names, begintime,yellow_return,yellow_like,server_return,server_like,mindata,mode):
    def __init__(self, config):
        # threading.Thread.__init__(self)
        self.serialNumberID = config.serial_numberID
        self.SiteID = config.serial_siteID
        self.position = config.serial_position
        self.MGDBSEND = config.server_MGDBSEND
        self.state = config.server_state
        # self.mqsave = mq.mqsave
        # self.img = img
        # self.workinput = workinput
        # self.user_names = user_names
        # self.begintime = begintime
        # self.mindata=mindata
        # self.yellow_return=yellow_return
        # self.yellow_like=yellow_like
        # self.server_return=server_return
        # self.server_like=server_like
        # self.mode=mode

    def start(self, img, workinput, user_names, begintime,yellow_return,yellow_like,server_return,server_like,mindata,mode):
        threading.Thread(target=self.run, args=(img, workinput, user_names, begintime,yellow_return,yellow_like,server_return,server_like,mindata,mode)).start()

    def run(self, img, workinput, user_names, begintime,yellow_return,yellow_like,server_return,server_like,mindata,mode):
        print "[Main] send run "
        # self.img = img
        # self.workinput = workinput
        # self.user_names = user_names
        # self.begintime = begintime
        # self.mindata = mindata
        # self.yellow_return = yellow_return
        # self.yellow_like = yellow_like
        # self.server_return = server_return
        # self.server_like = server_like
        # self.mode = mode
        # img = img
        # workinput = self.workinput
        # user_names = self.user_names
        # begintime = self.begintime
        # mindata=self.mindata
        # yellow_return = self.yellow_return
        # yellow_like = self.yellow_like
        # server_return = self.server_return
        # server_like = self.server_like
        # mode=self.mode
        try:
            # user_names=all_name[str(workinput)]
            user_names = "Unknown"
        except:
            pass
            # user_names=user_names
        print begintime
        # try:
        #     im = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # except:
        #     im=img
        # imImage = Image.fromarray(im)
        # output = StringIO()
        # imImage.save(output, format='JPEG')
        # im_data = output.getvalue()
        # print type(im_data)
        # payload1 = im_data.encode("base64")

        payload1 = opencv_base64(img)
        payload2 = opencv_base64(mindata)
        parsed_time = time.localtime(time.time())
        # now = time.ctime()
        # parsed_time = time.strptime(now)
        send_now = time.strftime("%Y-%m-%d", parsed_time)
        begintime1 = int(round(begintime * 1000))
        print begintime1
        data ={
            "CwrNo": str(workinput),
            "inTime": str(begintime1),
            "Date": str(send_now),
            "deviceId": str(self.serialNumberID),
            "data": payload1,
            "name": user_names,
            "Site_id": str(self.SiteID),
            "yellow_return": str(yellow_return),
            "yellow_like": str(yellow_like),
            "server_return": str(server_return),
            "server_like": str(server_like),
            "state": self.state,  # 0 in 1 out
            "mindata": payload2,
            "Position": self.position
        }
        if mode=="face":
            data["result"] = "1"
            data["emperor"] = "1"
        elif mode=="card":
            data["result"]="0"
            data["emperor"]="0"
        elif mode=="error":
            data["result"] = "0"
            data["emperor"] = "4"
            data["name"] = "Unknown"
        elif mode=="wan":
            data["name"]="臨時卡"
            data["result"] = "0"
            data["emperor"] = "2"
        elif mode=="face_err":
            data["result"] = "0"
            data["emperor"] = "1"
        elif mode=="greey_error":
            data["result"] = "0"
            data["emperor"] = "3"
            #data["name"] = "Unknown"
        data2=json.dumps(data)
        # main_logger.info("begin send img :" + str(begintime) + ".jpg")
        try:
            # print data2
            print "request ture sql"
            mqController.mqsave(data2, self.MGDBSEND)
            # main_logger.info("request ture sql")
            gc.collect()
            # r = requests.post(MGDB+MGDBSEND, json=data)
            # print str("ture send")+str(r.text)
        except Exception as err:
            print "ture send error"
            # main_logger.info("ture send error")
            # main_logger.info(str(err))
            print(err)

class server_socket(threading.Thread):

    def __init__(self, port):
        threading.Thread.__init__(self)
        self.port= port
        self.serverState = 0
        self.serverResume = 1

    def run(self):
        while 1:
            s = socket.socket()
            s.settimeout(3)
            status = s.connect_ex((str("220.246.46.207"), int(self.port)))
            if status == 0:
                self.serverState = 0
            else:
                print "[Main] Lose server connection"
                self.serverState = 1
                self.serverResume = 1
            time.sleep(5)

class yellow_box_connt(threading.Thread):
    def __init__(self, now_show_mode, config, logging):
        threading.Thread.__init__(self)
        self.now_show_mode = now_show_mode
        self.listen_seatus = config.server_listen_seatus
        self.mainLogger = logging
        # self.messageQueue = MQ
    def run(self):

        log_status = {
            "ciface_status": self.now_show_mode,  # dengji 0#found 1
        }
        log_status = json.dumps(log_status)
        try:
            mqController.mqsave(log_status, self.listen_seatus)
            self.mainLogger.info("mq ciface_status success")
        except Exception as err:
            self.mainLogger.info("mq ciface_status err:"+str(err))
            # self.mainLogger.info(err)
            # print err
            pass

def opencv_base64(img):
    try:
        im = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    except:
        im=img
    imImage = Image.fromarray(im)
    # imImage = cv2.cvtColor(imImage, cv2.COLOR_RGB2BGR)
    output = StringIO()
    imImage.save(output, format='JPEG')
    im_data = output.getvalue()
    payload1 = im_data.encode("base64")
    return payload1

def saveLocalImage(face, id, config):
    print "[Main] Save image"
    saveFace = face
    workerID = id
    localFaceDir = config.pbox_localFaceDir
    localImgCount = config.pbox_localImgCount
    if len(workerID) > 8:
        faceSubDir = str('3' + workerID[len(workerID) - 7:len(workerID)])
        facePath = str(localFaceDir + faceSubDir)
    try:
        os.mkdir(localFaceDir)
    except:
        pass

    try:
        os.mkdir(facePath)
    except:
        pass

    set_time = 0

    if len(os.listdir(facePath)) > localImgCount:
        for nbr in os.listdir(facePath):
            files = os.stat(facePath + "/" + str(nbr))
            if set_time == 0:
                set_time = files.st_mtime
                last = nbr
            if set_time > files.st_mtime:
                set_time = files.st_mtime
                last = nbr
                print last
        # mqsave("?employeeID=" + str(car_id) + "&imageNames=" + str(Serial_Number) + "_pic_" + str(last)[:-4] + "&serial_number=" + str(Serial_Number) + "&siteID=" + str(Site_id), mqdel_image_by_imageName)
        os.remove(facePath + "/" + str(last))
    # main_logger.info("save logic  time :" + str(begintime))
    saveTime = int(round(time.time() * 1000))
    cv2.imwrite(facePath + "/" + str(saveTime) + ".jpg", saveFace)
    # install_mq = {
    #     "employeeID": str(car_id),
    #     "imageNames": [str(Serial_Number) + "_pic_" + str(begintime)],
    #     "serial_number": str(Serial_Number),
    #     "siteID": Site_id
    # }
    # install_mq = json.dumps(install_mq)
    # try:
    #     print "mqsavedevice success "
    #     mqsave(install_mq, mqsavedevice)
    # except:
    #     print "mqsavedevice error"
    #     pass
    #
    # install_mqqq = {
    #     "imageNames": (str(Serial_Number) + "_pic_" + str(begintime)),
    #     "data": opencv_base64(imgxxx)
    # }
    # install_mqqqq = json.dumps(install_mqqq)
    # try:
    #     mqsave(install_mqqqq, mqsave_image)
    # except:
    #     print "mqsave error"
    #     pass
    #
    # return

def setupLogger(logger_name, log_file, level=logging.INFO):
	log = logging.getLogger(logger_name)
	formatter = logging.Formatter('%(asctime)s : %(message)s')
	fileHandler = logging.FileHandler(log_file, mode='a')
	fileHandler.setFormatter(formatter)
	streamHandler = logging.StreamHandler()
	streamHandler.setFormatter(formatter)

	log.setLevel(level)
	log.addHandler(fileHandler)
	log.addHandler(streamHandler)

def main():

    startUpScreen()

    localface = []
    serverFace = []
    state  = ''
    workerID = ''
    cardSenseTime = 0
    localScore = 0
    localScoreMin = 1000
    serverScore = 0

    config = pboxConfig()

    try:
        setupLogger('mainLogger', './main_log.txt')
        mainLogger = logging.getLogger('mainLogger')
    except Exception as err:
        print "[Main] mainLogger err: ", err

    list = workerList(config)

    server = server_socket(config.server_port)
    massageQueue = mqController(config, list, server)
    videoStream = WebcamVideoStream(src=0)
    nfc = nfcListener()
    screen = display(config.serial_number, videoStream, server)
    detection = FaceDetect(videoStream, screen, config)
    upLoadData = in_sql(config)
    light = lightAdjust(config, videoStream, detection)
    pboxConnection = yellow_box_connt("1", config, mainLogger)

    server.start()
    nfc.start()
    videoStream.start()
    detection.start()
    screen.start()
    massageQueue.start()
    light.start()
    pboxConnection.start()

    controller

    starttime = time.time()
    while 1:

        if workerID != '':

            if cardSenseTime:

                if time.time() - cardSenseTime < 5:

                    localScore, serverScore  = detection.getScore()
                    localFace, serverFace = detection.getFace()

                    if localScore < config.pbox_localPass:
                        mainLogger.info("Card number:"+str(workerID)+" serverScore:"+str(serverScore)+" localScore:"+str(localScore))
                        mainLogger.info("Card number:"+str(workerID)+" Local Pass")
                        state = 'pass'
                        screen.set('debug', 'lp')
                        screen.set('passScore', str(localScore))
                        upLoadData.start(serverFace, workerID, "Unknown", cardSenseTime, 0,localScore, 0, serverScore, localFace, "face")
                    elif serverScore > config.pbox_serverPass:
                        mainLogger.info("Card number:"+str(workerID)+" serverScore:"+str(serverScore) + " localScore:"+str(localScore))
                        mainLogger.info("Card number:"+str(workerID)+" Server pass")
                        state = 'pass'
                        screen.set('debug', 'sp')
                        screen.set('passScore', str(serverScore))
                        upLoadData.start(serverFace, workerID, "Unknown", cardSenseTime, 0, localScore,0, serverScore, localFace, "face")
                        if serverScore > 85:
                            saveFace = []
                            saveFace = detection.getSmallFace()
                            if saveFace != []:
                                try:
                                    saveLocalImage(saveFace, workerID, config)
                                except:
                                    pass
                    elif serverScore > config.pbox_serverSuspect:
                        mainLogger.info("Card number:"+str(workerID)+" serverScore:"+str(serverScore)+" localScore:"+str(localScore))
                        mainLogger.info("Card number:" + str(workerID)+" Server suspect")
                        state = 'suspect'
                        screen.set('passScore', str(serverScore))
                        upLoadData.start(serverFace, workerID, "Unknown", cardSenseTime, 0, localScore, 0, serverScore, localFace, "face")
                    elif serverScore > 0:
                        mainLogger.info("Card number:"+str(workerID)+" serverScore:"+str(serverScore)+" localScore:"+str(localScore))
                        mainLogger.info("Card number:"+str(workerID)+" Server fault")
                        state = 'fault'
                        upLoadData.start(serverFace, workerID, "Unknown", cardSenseTime, 0, localScore, 0, serverScore, localFace, "face_err")
                    else:
                        pass

                    if state != '':
                        detection.clear()
                        screen.set('state', state)
                        if state == 'pass' or state == 'suspect':
                            subprocess.call('aplay /opt/main/remoteFirefly/pbox/sound/beep-good.wav &', shell=True)
                        elif state == 'fault':
                            subprocess.call('aplay /opt/main/remoteFirefly/pbox/sound/beep-bad.wav &', shell=True)
                        time.sleep(2)
                        state = ''
                        workerID = ''
                        cardSenseTime = 0
                        localScore = 0
                        localScoreMin = 1000
                        screen.clear()

                    if localScore < localScoreMin:
                        localScoreMin = localScore
                else:
                    workerID = ''
                    cardSenseTime = 0
                    detection.clear()
                    localScore = 0
                    localScoreMin = 1000
                    screen.set('state', 'fault')
                    screen.set('debug', 'timeOut')
                    subprocess.call('aplay /opt/main/remoteFirefly/pbox/sound/beep-bad.wav &', shell=True)
                    time.sleep(2)
                    screen.clear()
                    mainLogger.info("Card number:"+str(workerID)+" Timeout fault")
            else:
                mainLogger.info("Recive NFC number: " + str(workerID))
                screen.set('workerID', workerID)
                screen.set('state', 'working')

                # Check Green Card validation if checkGreenExp is true
                if list.checkGreenExp:
                    if workerID in list.greenCardExpiry:
                        if (time.time() > float(list.greenCardExpiry[workerID])):
                            screen.set('state', 'greenCardExpiry')
                            subprocess.call('aplay /opt/main/remoteFirefly/pbox/sound/beep-bad.wav &', shell=True)
                            upLoadData.start(detection.img, workerID, "Unknown", time.time(), None, None, None, None, detection.img, "greey_error")
                            time.sleep(2)
                            screen.clear()
                            workerID = ''
                            continue
                        else:
                            pass
                    else:
                        pass
                else:
                    pass

                # Check workerID validation and start recognition
                if workerID in list.CWRCard:
                    mainLogger.info("Card number:"+str(workerID)+" valid in CWR list")
                    detection.setID(workerID)
                    cardSenseTime = time.time()
                elif workerID in list.whiteCard:
                    mainLogger.info("Card number:"+str(workerID)+" valid in white list")
                    detection.setID(workerID)
                    cardSenseTime = time.time()
                elif workerID in list.tempCard:
                    mainLogger.info("Card number:"+str(workerID)+" valid in temp list")
                    screen.set('state', 'temp')
                    subprocess.call('aplay /opt/main/remoteFirefly/pbox/sound/beep-good.wav &', shell=True)
                    upLoadData.start(detection.img, workerID, "臨時卡", time.time(), None, None, None, None,detection.img, "wan")
                    time.sleep(2)
                    screen.clear()
                    workerID = ''
                else:
                    mainLogger.info("Card number:"+str(workerID)+" has not registered yet")
                    screen.set('state', 'norecord')
                    subprocess.call('aplay /opt/main/remoteFirefly/pbox/sound/beep-bad.wav &', shell=True)
                    upLoadData.start(detection.img, workerID, "Unknown", time.time(), None, None, None, None, detection.img, "error")
                    time.sleep(2)
                    screen.clear()
                    workerID = ''
                    list.update()

        else:
            workerID = nfc.get_ID()
            nfc.clear_ID()
            if workerID[:3] != "CWR":
                if workerID in list.CWRCard:
                    workerID = list.serialMatching[str(workerID)]


        # #=====Data Scan=====
        # workerID_buf = nfc.get_ID()
        # recognitionFace_buf, face_buf = detection.get_face()
        # localScore = recognition.getScore_local()
        # serverScore = recognition.getScore_server()
        # #=====Data manipulation=====
        # if localScore != 0:
        #     if faceRecognitionTime != 0:
        #         if localScore >= 300:
        #             if time.time() - cardSenseTime >= 5:
        #                 screen.setAtr('state', 'fault')
        #                 subprocess.call('aplay sound/beep-bed.wav', shell=True)
        #             else:
        #                 # localScore = 0
        #                 # recognition.clear_score()
        #                 faceRecognitionTime = 0
        #         elif localScore >= 150:
        #             screen.setAtr('state', 'suspect')
        #             print "[Main] Recognition result: suspect"
        #             subprocess.call('aplay sound/beep-good.wav', shell=True)
        #         elif localScore < 150:
        #             screen.setAtr('state', 'pass')
        #             print "[Main] Recognition result: pass"
        #             subprocess.call('aplay sound/beep-good.wav', shell=True)
        #         else:
        #             # localScore = 0
        #             # recognition.clear_score()
        #             faceRecognitionTime = 0
        #             print "[Main] Recognition score error"
        #
        #         if faceRecognitionTime != 0:
        #             print "[Main] Recognition score:", localScore
        #             workerID = ''
        #             recognition.clear_score()
        #             cardSenseTime = 0
        #             faceRecognitionTime = 0
        #             time.sleep(2)
        #             screen.setAtr('state', '')
        #             screen.setAtr('workerID', workerID)
        #     else:
        #         pass
        #     recognition.clear_score()
        # else:
        #     pass
        #
        # if serverScore != 0:
        #     if serverScore >= 70:
        #         print "[Main] Server result: pass"
        #         screen.setAtr('state', 'pass')
        #         subprocess.call('aplay sound/beep-good.wav', shell=True)
        #     elif serverScore >=60:
        #         print "[Main] Server result: suspect"
        #         screen.setAtr('state', 'suspect')
        #         subprocess.call('aplay sound/beep-good.wav', shell=True)
        #     elif serverScore > 0:
        #         print "[Main] Server result: fault"
        #         screen.setAtr('state', 'fault')
        #         subprocess.call('aplay sound/beep-bed.wav', shell=True)
        #
        #     workerID = ''
        #     recognition.clear_score()
        #     cardSenseTime = 0
        #     faceRecognitionTime = 0
        #     time.sleep(2)
        #     screen.setAtr('state', '')
        #     screen.setAtr('workerID', workerID)
        # else:
        #     pass
        #
        # if workerID_buf != '':
        #     if workerID != '':
        #         nfc.clear_ID()
        #     else:
        #         print "[Main] NFC card activate"
        #         workerID = workerID_buf
        #         cardSenseTime = time.time()
        #         nfc.clear_ID()
        #         screen.setAtr('state', 'working')
        #         screen.setAtr('workerID', workerID)
        # else:
        #     pass
        #
        # if face_buf != []:
        #     # print "[Main] Face activate"
        #     face = face_buf
        #     recognitionFace = recognitionFace_buf
        #     faceSenseTime = time.time()
        #     screen.setAtr('face', face)
        #     detection.clear_face()
        # else:
        #     if faceSenseTime != 0:
        #         if time.time() - faceSenseTime > 1:
        #             # print "[Main] Face timeout"
        #             face = []
        #             recognitionFace = []
        #             faceSenseTime = 0
        #     else:
        #         pass
        #
        # if cardSenseTime != 0:
        #     if time.time() - cardSenseTime <= 5:
        #         if workerID in workerList.worker:
        #             if faceRecognitionTime != 0:
        #                 if time.time() - faceRecognitionTime >= 3:
        #                     pass
        #                     # workerID = ''
        #                     # cardSenseTime = 0
        #                     # faceRecognitionTime = 0
        #                     # screen.setAtr('state', 'fault')
        #                     # subprocess.call('aplay sound/beep-bad.wav', shell=True)
        #                     # time.sleep(2)
        #                     # screen.setAtr('workerID', workerID)
        #                     # screen.setAtr('state', '')
        #                 else:
        #                     pass
        #             else:
        #                 if recognitionFace != []:
        #                     print "[Main] Recognition using small pic"
        #                     faceRecognitionTime = time.time()
        #                     recognition.start(workerID, recognitionFace)
        #                     recognitionFace = []
        #                     face = []
        #                 else:
        #                     print "[Main] Recognition using large pic"
        #                     faceRecognitionTime = time.time()
        #                     recognition.start(workerID, screen.get_img())
        #         else:
        #             workerID = ''
        #             cardSenseTime = 0
        #             screen.setAtr('state', 'norecord')
        #             subprocess.call('aplay sound/beep-bad.wav', shell=True)
        #             time.sleep(2)
        #             screen.setAtr('workerID', workerID)
        #             screen.setAtr('state', '')
        #             print "[Main] No cardID record"
        #     else:
        #         if faceRecognitionTime != 0:
        #             if time.time() - faceRecognitionTime >=3:
        #                 # workerID = ''
        #                 # cardSenseTime = 0
        #                 faceRecognitionTime = 0
        #                 # screen.setAtr('state', 'fault')
        #                 # time.sleep(1)
        #                 # screen.setAtr('workerID', workerID)
        #                 # screen.setAtr('state', '')
        #                 print "[Main] Recognition time out and no success score return"
        #             else:
        #                 pass
        #         else:
        #             print "[Main] CardSense time out and recognition not start"
        #
        #         if faceRecognitionTime == 0:
        #             workerID = ''
        #             cardSenseTime = 0
        #             # faceRecognitionTime = 0
        #             screen.setAtr('state', 'fault')
        #             subprocess.call('aplay sound/beep-bad.wav', shell=True)
        #             time.sleep(2)
        #             screen.setAtr('workerID', workerID)
        #             screen.setAtr('state', '')
        # else:
        #     pass

        # if time.time()-starttime > 5000:
        #     break
        time.sleep(0.1)
    nfc.stop()
    screen.stop()
    detection.stop()
    videoStream.stop()

if __name__ == '__main__':

    main()

    # threads = []
    # t1 = threading.Thread(target=screen.standDraw, args=())
    # t2 = threading.Thread(target=screen.dynamicDraw, args=())
    # t3 = threading.Thread(target=mainGo, args=())
    # threads.append(t1)
    # threads.append(t2)
    # threads.append(t3)
    # for t in threads:
    #     t.setDaemon(True)
    #     t.start()
    # t1.setDaemon(True)
    # t1.start()