# -*- coding: utf-8 -*-
import requests
import ConfigParser
import os
import base64
import time
import sys

class Change_device():
    def Change_device_startup(self):
        cp_pbox = ConfigParser.SafeConfigParser()
        cp_server = ConfigParser.SafeConfigParser()
        cp_serial_number= ConfigParser.SafeConfigParser()
        cp_pbox.read('./pbox_config/pbox.ini')
        cp_serial_number.read('./pbox_config/serial_number.ini')
        cp_server.read('./pbox_config/server.ini')

        siteID_old = cp_pbox.get('download', 'siteid_old')
        serial_number_old = cp_pbox.get('download', 'serial_number_old')
        siteID_new = cp_serial_number.get('inset', 'siteid')
        serial_number_new = cp_serial_number.get('inset', 'serial_number')
        mqdb = cp_server.get('backend', 'server')
        get_device_url = cp_server.get('mq', 'device_url')
        get_update_url = cp_server.get('mq', 'update_url')
        get_log = cp_server.get('mq', 'get_log')
        try:
            device_url = mqdb + get_device_url + '?siteID=' + siteID_old + '&serial_number=' + serial_number_old
            images_url = mqdb + get_log + "?logName="
            update_url = mqdb + get_update_url + '?siteID_old=' + siteID_old + '&serial_number_old=' + serial_number_old + '&siteID_new=' + siteID_new + '&serial_number_new=' + serial_number_new
            rq = requests.get(device_url)
            if rq.status_code!=200:
                print '网络异常，无法获取工号'
            else:
                xx = rq.json()
                if len(xx) != 0:
                    try:
                        os.mkdir(r'./cidevice_face')
                    except:
                        pass
                    for i in xx:
                        employeeID = i["employeeID"]
                        try:
                            os.makedirs(r'./cidevice_face/' + employeeID)
                        except:
                            pass
                        imageNames = i["imageNames"]
                        for j in imageNames:
                            imageName = j
                            get_images_url_1 = images_url + imageName
                            rs = requests.get(get_images_url_1)
                            downLoad_pic_name=j.split("_pic_")[1]
                            if rs.status_code==200:
                                print '图片下载中....'
                                x = rs.json()["data"]
                                xall = base64.b64decode(x)
                                filex = open("./cidevice_face/" + "/" + employeeID + "/" + str(downLoad_pic_name) + ".jpg", "wb")
                                filex.write(xall)
                                filex.close()
                                time.sleep(1)
                            else:
                                print '网络异常，图片下载中断....'
                    print '图片已加载完毕'
                    try:
                        rs1 = requests.get(update_url)
                        if rs1.status_code==200:
                            get_update_json = rs1.text
                            print '数据库更新' + str(get_update_json)
                        else:
                            print '网络异常，无法更新数据库'
                    except Exception as err:
                        print err
                        print '无法更新数据库'
                    cp_pbox.set('download', 'siteID_old', '')
                    cp_pbox.set('download', 'serial_number_old', '')
                    cp_pbox.write(open('./pbox_config/pbox.ini', 'w'))
                    cp_pbox.write(sys.stdout)
                    print 'siteID_old、serial_number_old已置空'
                else:
                    print '请输入正确的siteID_old、serial_number_old'
        except Exception as err:
            print err
            print '更新失败！网络异常或者数据库中无该照片'
