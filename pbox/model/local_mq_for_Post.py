# -*- coding: utf-8 -*-
import pika
import time
import requests
import json
import ConfigParser
import threading

class Local_MQ_for_Post(threading.Thread):
    def __init__(self,mgDB,mgURL):
        threading.Thread.__init__(self)
        self.mgDB=mgDB
        self.mgURL=mgURL

    def run(self):
        while True:
            try:
                credentials = pika.PlainCredentials('yuanwendong', '123456')
                parameters = pika.ConnectionParameters('localhost', 5672, '/', credentials)
                parameters.socket_timeout = 5
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                queueName = self.mgURL
                # 定义队列
                channel.queue_declare(queue=queueName, durable=True)
                print 'waiting for' + queueName
                channel.basic_qos(prefetch_count=1)
                channel.basic_consume(self.rec,
                                      queue=queueName,
                                      # 这里就不用再写no_ack=False了
                                      )
                channel.start_consuming()
            except Exception as err:
                print err
                print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(
                    time.time())) + ':' + self.mgURL + ',losed conect,try to new connection'
                time.sleep(5)

    def rec(self, ch, method, properties, body):
        try:
            data = json.loads(body)
            while True:
                try:

                    break
                except:
                    print 'ack err'
                    time.sleep(2)
                    continue
            while True:
                try:
                    r = requests.post(self.mgDB + self.mgURL, json=data, timeout=10)
                    ch.basic_ack(delivery_tag=method.delivery_tag)
                    if r.status_code == 200:
                        print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(
                            time.time())) + ':' + self.mgURL + 'Done' + 'server return:' + r.text
                        break
                    else:
                        print 'post err'
                        time.sleep(2)
                        continue
                except:
                    time.sleep(2)
                    continue
        except Exception as err:
            print err
            print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(
                time.time())) + ':' + self.mgURL + 'timeout'
            time.sleep(2)
if __name__ == '__main__':
    # cp = ConfigParser.SafeConfigParser()
    # cp.read('../conf.ini')
    # MGDB = cp.get('info', 'MGDB')
    # MGDBSEND = cp.get('info', 'MGDBSEND')
    MGDB = 'https://ptahtst1.cisystemsolutions.com/api/V1'
    MGDBSEND = '/in'
    t1=Local_MQ_for_Post(MGDB,MGDBSEND)
    t1.start()
    print "run 10086"