# -*- coding: utf-8 -*-
import pika
import time
import requests
import json
import ConfigParser
import threading
import os

class Local_MQ_ybStatus(threading.Thread):
    def __init__(self,mgDB,mgURL):
        threading.Thread.__init__(self)
        self.mgDB=mgDB
        self.mgURL=mgURL
    def run(self):
        while True:
            try:
                credentials = pika.PlainCredentials('yuanwendong', '123456')
                parameters = pika.ConnectionParameters('localhost', 5672, '/', credentials)
                parameters.socket_timeout = 5
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                queueName = self.mgURL
                # 定义队列
                channel.queue_declare(queue=queueName, durable=True)
                print 'waiting for' + queueName
                channel.basic_qos(prefetch_count=1)
                channel.basic_consume(self.rec,
                                      queue=queueName,
                                      # 这里就不用再写no_ack=False了
                                      # no_ack=True
                                      )
                channel.start_consuming()
                print "send run "
            except Exception as err:

                print err
                print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(
                    time.time())) + ':' + self.mgURL + ',losed conect,try to new connection'
                time.sleep(5)
    def rec(self,ch,method, properties, body):
            try:
                print(self.mgURL+'Done')
                print body
                # print type(body)
                # print body["mg1"]
                # print body["mg2"]
                data =json.loads(body)
                ciface_status= data["ciface_status"]
                DateTime = str(int(round(time.time() * 1000)))
                RAM = os.popen('free').read().split()[7:10]
                RAM0 = float(RAM[0]) / 1024
                RAM1 = float(RAM[1]) / 1024
                percent = RAM1 / RAM0 * 100
                RAM_percent='%.2f' % percent + '%'
                time1 = os.popen('cat /proc/stat').readline().split()[1:5]
                time.sleep(0.2)
                time2 = os.popen('cat /proc/stat').readline().split()[1:5]
                deltaUsed = int(time2[0]) - int(time1[0]) + int(time2[2]) - int(time1[2])
                deltaTotal = deltaUsed + int(time2[3]) - int(time1[3])
                cpuUsage = float(deltaUsed) / float(deltaTotal) * 100
                CPU='%.1f' % cpuUsage + '%'
                if data:
                    cp_serial_number = ConfigParser.SafeConfigParser()
                    cp_serial_number.read('./pbox_config/serial_number.ini')
                    serial_number_id = cp_serial_number.get('inset', 'serial_number_id')
                    siteid=cp_serial_number.get('inset', 'siteid')
                    data1={
                            "Device_Id":serial_number_id,
                            "Site_id":siteid,
                            "CPU":CPU,
                            "RAM": RAM_percent,
                            "temperature": "null",
                            "ciface_status":ciface_status,
                            "DateTime": DateTime
                    }
                    while True:
                        try:
                            ch.basic_ack(delivery_tag=method.delivery_tag)
                            break
                        except:
                            time.sleep(2)
                            print 'ack err'
                            continue
                    while True:
                        try:
                            r = requests.post(self.mgDB+self.mgURL,json=data1,timeout=10)
                            if r.status_code == 200:
                                print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(
                                    time.time())) + ':' + self.mgURL + 'Done' + 'server return:' + r.text
                                break
                            else:
                                print 'post err'
                                time.sleep(2)
                                continue
                        except:
                            time.sleep(2)
                            continue
            except Exception as err:
                print err
                print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(
                    time.time())) + ':' + self.mgURL + 'timeout'
                time.sleep(2)
# if __name__ == '__main__':
#     cp = ConfigParser.SafeConfigParser()
#     cp.read('../conf.ini')
#     MGDB = cp.get('info', 'MGDB')
#     MGDBSEND = cp.get('info', 'MGDBSEND')
#     t1=Local_MQ_ybStatus(MGDB,MGDBSEND)
#     t1.start()
#     print "run 10086"