# -*- coding: utf-8 -*-
import pika
import zipfile
import os
import json
import base64
import time
import threading
import ConfigParser
class Computer(threading.Thread):
    def __init__(self,deviceID):
        threading.Thread.__init__(self)
        self.deviceID=deviceID
    def run(self):
        while True:
            try:
                cp = ConfigParser.SafeConfigParser()
                cp.read('conf.ini')
                server_ip = cp.get('mq', 'server_ip')
                server_port = int(cp.get('mq', 'server_port'))
                server_username = cp.get('mq', 'server_username')
                server_password = cp.get('mq', 'server_password')
                server_vhost = cp.get('mq', 'server_vhost')
                credentials = pika.PlainCredentials(server_username, server_password)
                parameters = pika.ConnectionParameters(server_ip, server_port,server_vhost, credentials)
                parameters.socket_timeout = 1
                self.connection = pika.BlockingConnection(parameters)
                self.channel = self.connection.channel()
                # 定义队列
                queueName=self.deviceID
                self.channel.queue_declare(queue=queueName,durable=True)
                print self.deviceID+' [*] Waiting for upgration'
                self.channel.basic_qos(prefetch_count=1)
                self.channel.basic_consume(self.request, queue=queueName)
                self.channel.start_consuming()
            except Exception as err:
                print err
                print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())) + ':' + self.deviceID + ',losed conect,try to new connection'
                time.sleep(5)
    def release_zip(self):
        file_list = os.listdir(r'.')
        print file_list
        for file_name in file_list:
            if os.path.splitext(file_name)[1] == '.zip':
                print file_name
                file_zip = zipfile.ZipFile(file_name, 'r')
                for file in file_zip.namelist():
                    file_zip.extract(file, r'.')
                file_zip.close()
                os.remove(file_name)
    # 定义接收到消息的处理方法
    def request(self,ch, method, properties, body):
        file = open('zip1.zip', 'wb')
        try:
            encodedjson = json.dumps(body)
            a = json.loads(encodedjson)
            filedata = base64.b64decode(a)
            file.write(filedata)
            self.release_zip()
            print "run 1"
            newfile = "sh test.sh"
            p = os.popen(newfile)
            response = p.read()
            print response
            print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))+'in response'+self.deviceID
            # 将计算结果发送回控制中心
            ch.basic_publish(exchange='',
                             routing_key=properties.reply_to,
                             body=response)
            ch.basic_ack(delivery_tag=method.delivery_tag)
            p.close()
        except Exception as err:
            print err
        finally:
            file.close()
# if __name__ == '__main__':
#     computer=Computer('CHINA_A')